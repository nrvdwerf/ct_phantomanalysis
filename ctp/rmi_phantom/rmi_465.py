# main document for CT phantom analysis

import pydicom
# import SimpleITK as sitk
import os
import numpy as np
# import scipy
import matplotlib.pyplot as plt
# import cv2
import math
import statistics
from ctp.Phantom_analysis_toolbox import Phantom_analysis_toolbox as pat
from ctp.rmi_phantom import constants as C

  

    
class RMI_465_analyser(pat):
    def __init__(self, used_path, plot_centers = False):
        super().__init__(used_path)
        
        self.insert_size = math.pi * (30/2)**2 / self.pixel_size**2
        self.high_density_insert_HU = 1100
        self.WAD_output = {}        
        self.RMI_465_center_slice()
        self.insert_ROI = int((15/2) / self.pixel_size)
        
        # #flip array if scan direction is flipped
        # self.CIRS_062_flipper()
        
        self.RMI_465_object_locater(plot_centers)
        self.RMI_465_measurements()
        
        return None
    
    @staticmethod
    def points_closest(points, test_coordinate, K):
 
        points.sort(key = lambda K: (K[0] - test_coordinate[0])**2 + (K[1] - test_coordinate[1])**2)
     
        return points[:K]
    
    
    def RMI_465_center_slice(self):        
        array = self.array.copy()
        
        in_air_slices = []
        for slice_index in range(array.shape[2]):

            sphere_centers = pat.find_circle(self.array[:,:,slice_index],-800, self.insert_size,\
                                             physic_size_est = 0.4, kernel = 3, sphere_est = 0.8)
            if len(sphere_centers) == 20:
                in_air_slices.append(slice_index)
                
        self.in_air_slice = int(statistics.median(in_air_slices))
            
        sphere_centers = pat.find_circle(self.array[:,:,self.in_air_slice],-800, self.insert_size,\
                                             physic_size_est = 0.4, kernel = 3, sphere_est = 0.8)
        centers = []
        for key in sphere_centers.keys():
            centers.append([sphere_centers[key][1], sphere_centers[key][0]])
            
        self.insert_centers = centers
        
        north = self.points_closest(centers, [0,int(self.header.Rows/2)], 1)
        east = self.points_closest(centers, [int(self.header.Columns/2),int(self.header.Rows)], 1)
        south = self.points_closest(centers, [int(self.header.Columns),int(self.header.Rows/2)], 1)
        west = self.points_closest(centers, [int(self.header.Columns/2),0], 1)
        
        self.center_phantom = [north[0][0] + int((south[0][0] - north[0][0]) / 2),\
                       west[0][1] + int((east[0][1] - west[0][1]) / 2)]
            
        array = self.array.copy()
        mask = pat.create_circular_mask(self.header.Columns, self.header.Rows, self.center_phantom, int(20 / self.header.PixelSpacing[0]))
        
        in_phantom_slices = []
        for slice_index in range(array.shape[2]):
            ROI_mean = np.ma.masked_equal(mask * array[:,:,slice_index], 0).mean()
            
            if ROI_mean > -200:
                in_phantom_slices.append(slice_index)
                
        self.in_phantom_slice = int(statistics.median(in_phantom_slices))
        
        return None
    
    
    # def CIRS_062_flipper(self):
    #     center_low = self.array[int(self.header.Columns/2),int(self.header.Rows/2),5:self.array.shape[2]-5].min()
        
    #     index_center_low, = np.where(self.array[int(self.header.Columns/2),int(self.header.Rows/2),:] == center_low)
        
    #     if len(index_center_low) > 1:
    #         index_center_low = index_center_low[0]
        
    #     if index_center_low > self.center_slice:
    #         self.array = np.flip(self.array,2)
    #         self.CIRS_062_center_slice()

    #     return None                                    
    
    @staticmethod 
    def add_mask(columns, rows, obj_center, mask_in, array_max, mask_size = 3):
        
        mask_new = pat.create_circular_mask(columns, rows, obj_center , mask_size)
        mask_new = mask_new * np.ones_like(mask_new) * 2 * array_max
        
        mask_out = mask_in + mask_new
        
        return mask_out
    
    
    @staticmethod
    def object_dict_init(inner_obj, outer_obj):
        object_dict = {}
        
        dict_keys_inner = ['inner_top', 'inner_upper_right', 'inner_right', 'inner_lower_right',\
                           'inner_bottom', 'inner_lower_left', 'inner_left', 'inner_upper_left']
        dict_keys_outer = ['outer_top', 'outer_upper_right', 'outer_right', 'outer_lower_right',\
                           'outer_bottom', 'outer_lower_left', 'outer_left', 'outer_upper_left']
        
        for index in range(len(dict_keys_inner)):
            object_dict[dict_keys_inner[index]] = [inner_obj[index]]
        
        for index in range(len(dict_keys_outer)):
            object_dict[dict_keys_outer[index]] = [outer_obj[index]]    
            
        return object_dict  
    
    @staticmethod
    def clean_center_list(centers, results_dict):
        for key in results_dict:
            try:
                centers.remove(results_dict[key][0])
            except:
                pass
            
        return centers

    
    def RMI_465_object_locater(self, plot_centers = False):
        centers = self.insert_centers
        
        results_ROI = {}
        results_ROI['ROI_1'] = self.points_closest(centers, [0,int(self.header.Rows/2)], 1)
        results_ROI['ROI_9'] = self.points_closest(centers, [int(self.header.Columns/2),0], 1)
        results_ROI['ROI_12'] = self.points_closest(centers, [int(self.header.Columns/2),int(self.header.Rows)], 1)
        results_ROI['ROI_20'] = self.points_closest(centers, [int(self.header.Columns),int(self.header.Rows/2)], 1)
        
        centers = self.clean_center_list(centers, results_ROI)
            
        tmp_points = self.points_closest(centers, [0,int(self.header.Rows/2)], 2)
        results_ROI['ROI_2'] = self.points_closest(tmp_points, [0,0], 1)
        results_ROI['ROI_3'] = self.points_closest(tmp_points, [0,self.header.Rows], 1)
        centers = self.clean_center_list(centers, results_ROI)
        
        tmp_points = self.points_closest(centers, [int(self.header.Columns/2),0], 2)
        results_ROI['ROI_5'] = self.points_closest(tmp_points, [0,0], 1)
        results_ROI['ROI_13'] = self.points_closest(tmp_points, [self.header.Columns,0], 1)
        centers = self.clean_center_list(centers, results_ROI)
        
        tmp_points = self.points_closest(centers, [int(self.header.Columns/2),self.header.Rows], 2)
        results_ROI['ROI_8'] = self.points_closest(tmp_points, [0,self.header.Rows], 1)
        results_ROI['ROI_16'] = self.points_closest(tmp_points, [self.header.Columns,self.header.Rows], 1)
        centers = self.clean_center_list(centers, results_ROI)        
        
        tmp_points = self.points_closest(centers, [self.header.Columns,int(self.header.Rows/2)], 2)
        results_ROI['ROI_18'] = self.points_closest(tmp_points, [self.header.Columns,0], 1)
        results_ROI['ROI_19'] = self.points_closest(tmp_points, [self.header.Columns,self.header.Rows], 1)
        centers = self.clean_center_list(centers, results_ROI)        
                
        results_ROI['ROI_4'] = self.points_closest(centers, [0,int(self.header.Rows/2)], 1)
        results_ROI['ROI_10'] = self.points_closest(centers, [int(self.header.Columns/2),0], 1)
        results_ROI['ROI_11'] = self.points_closest(centers, [int(self.header.Columns/2),int(self.header.Rows)], 1)
        results_ROI['ROI_17'] = self.points_closest(centers, [int(self.header.Columns),int(self.header.Rows/2)], 1)
        centers = self.clean_center_list(centers, results_ROI)  
        
        results_ROI['ROI_6'] = self.points_closest(centers, [0,0], 1)
        results_ROI['ROI_7'] = self.points_closest(centers, [0,self.header.Rows], 1)
        results_ROI['ROI_14'] = self.points_closest(centers, [self.header.Columns,0], 1)
        results_ROI['ROI_15'] = self.points_closest(centers, [self.header.Columns,self.header.Rows], 1)
        
        ROI_results = {}
        array = self.array[:,:,self.in_phantom_slice].copy()
        mask = np.zeros_like(array)
        for index in range(1,21):
            key = 'ROI_' + str(index)
            ROI_results[key] = results_ROI[key]
            mask = self.add_mask(self.header.Columns, self.header.Rows,\
                                 results_ROI[key][0], mask, array.max()) 
            
        self.results_ROIs = ROI_results
        
        if plot_centers:
            plt.subplot(131)
            plt.imshow(array)
            plt.subplot(132)
            plt.imshow(mask)
            plt.subplot(133)
            plt.imshow(array + mask)
            plt.show()
            
        self.WAD_output['image'] = array + mask
        
        return None
    
    
    def RMI_465_measurements(self):
        # measure mean and SD for each of the objects
        size_obj = self.insert_ROI
        
        # slices to measure in
        min_slice = self.in_phantom_slice - 1
        max_slice = self.in_phantom_slice + 1
        
        for key in self.results_ROIs.keys():
            if 'image' not in key:
                center = self.results_ROIs[key][0]
                
                mean, SD = pat.calc_mean_SD(self.array[:,:,min_slice:max_slice],\
                                                [center[1], center[0]], size_obj)
                
                
                self.results_ROIs[key].append(mean)
                self.results_ROIs[key].append(SD)
                self.WAD_output[key + '_mean'] = mean
                self.WAD_output[key + '_SD'] = SD
                
        return None
        
        
def dcm_list_builder(path, test_text = ""):
    # function to get list of dcm_files from dcm directory
    dcm_path_list = []
    for (dirpath, dirnames, filenames) in os.walk(path,topdown=True):
        if dirpath not in dcm_path_list:
            for filename in filenames:
                try:
                    tmp_str = str('\\\\?\\' + os.path.join(dirpath, filename))
                    pydicom.read_file(tmp_str, stop_before_pixels = True)
                    if dirpath not in dcm_path_list:
                        dcm_path_list.append(dirpath)
                except:
                    pass
            else:
                pass
    return dcm_path_list