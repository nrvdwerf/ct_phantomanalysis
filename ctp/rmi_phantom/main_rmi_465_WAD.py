from ctp.rmi_phantom import rmi_465

def main_rmi465_wad(dcm=None, manual_input=None,\
                    action_name=None, action_params=None):    
    
    if action_name != 'AnalyseRMI465':
        raise RuntimeError('I can only analyse RMI465 phantom!')

    rmi465_analysis = rmi_465.RMI_465_analyser(dcm)

    return rmi465_analysis.WAD_output
    
    
if __name__ == '__main__':
    pass