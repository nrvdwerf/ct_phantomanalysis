from ctp.rmi_phantom import rmi_465
from ctp.rmi_phantom import constants as C
from tqdm import tqdm
import numpy as np
np.seterr(all='ignore')
import matplotlib.pyplot as plt
    

def main():
    rmi_analysis = rmi_465.RMI_465_analyser(dcm_path_list[idx], plot_centers = True)
    
    return rmi_analysis
    
    
if __name__ == '__main__':
    dcm_path_list = rmi_465.dcm_list_builder(C.root_path)
    print('Used config = ', C.configuration)
    
    for idx in tqdm(range(len(dcm_path_list))):
        dcm = main()