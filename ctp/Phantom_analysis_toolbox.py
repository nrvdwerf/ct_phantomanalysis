import pydicom
import cv2
import scipy
import numpy as np
import os
import scipy.signal
import matplotlib.pyplot as plt
import math
import copy
from scipy.interpolate import RegularGridInterpolator as rgi
from skimage.transform import rotate
import scipy.interpolate as inter
import xlsxwriter
from datetime import date
from sys import platform

class Phantom_analysis_toolbox():
    def __init__(self, used_path):
        self.path = used_path
        if type(used_path) == str:
            self.dcm_list_construct() 
        else:
            self.dcm_files = used_path
        
        self.dcm_reader()
        self.pixel_size_calc()
        self.slice_thickness = self.header.SliceThickness
        
        return None
    
    
    def dcm_list_construct(self):
        #read dcm_files from path
        #path certainly contains DCM files, as tested by dcm_list_builder function
        dcm_path = self.path
        
        dcm_files = []
        for (dirpath, dirnames, filenames) in os.walk(dcm_path,topdown=False):
            for filename in filenames:
                try:
                    if not filename == 'DIRFILE':   
                        if 'win' in platform:
                            dcm_file = str('\\\\?\\' + os.path.join(dirpath, filename))
                        else:
                            dcm_file = os.path.join(dirpath, filename)
                        pydicom.read_file(dcm_file, stop_before_pixels = True)
                        dcm_files.append(dcm_file)
                except:
                    pass
                
        self.dcm_files = dcm_files
        
        return None
    

    def dcm_reader(self):
        dcm_files = self.dcm_files
    
        read_RefDs = True
        while read_RefDs:
            for index in range(len(dcm_files)):
                try:
                    RefDs = pydicom.read_file(dcm_files[index], stop_before_pixels = False)
                    read_RefDs = False
                    break
                except:
                    pass
        
        slice_thick_ori = RefDs.SliceThickness
        
        ConstPixelDims = (int(RefDs.Rows), int(RefDs.Columns), len(dcm_files))
        dcm_array = np.zeros([ConstPixelDims[0],ConstPixelDims[1],len(dcm_files)],\
                              dtype=RefDs.pixel_array.dtype) 
    
        instances = []    
        for filenameDCM in dcm_files:
            try:
                ds = pydicom.read_file(filenameDCM, stop_before_pixels = True)
                instances.append(int(ds.InstanceNumber))
            except:
                pass
        
        instances.sort()

        index = 0
        for filenameDCM in dcm_files:
            try:
                ds = pydicom.read_file(filenameDCM)
                dcm_array[:,:,instances.index(ds.InstanceNumber)] = ds.pixel_array
                if ds.InstanceNumber in instances[:2]:
                    if ds.InstanceNumber == instances[0]:
                        loc_1 = ds.SliceLocation
                    else:
                        loc_2 = ds.SliceLocation
                index += 1
                if ds.InstanceNumber == min(instances):
                    self.min_loc = ds.SliceLocation
                elif ds.InstanceNumber == max(instances):
                    self.max_loc = ds.SliceLocation
            except:
                pass
            
        try:
            RefDs.SliceThickness = abs(loc_1 - loc_2)
        except:
            pass
            
            
        dcm_array = dcm_array * RefDs.RescaleSlope + RefDs.RescaleIntercept
        
        self.header = RefDs
        self.array = dcm_array
        self.slice_thick_ori = slice_thick_ori
        return None
    
    
    def pixel_size_calc(self):
        try:
            self.pixel_size = self.header.PixelSpacing[0]
        except:
            FOV = self.header.ReconstructionDiameter
            matrix_size = self.header.Rows
        
            self.pixel_size = FOV / matrix_size            
        return None
    
    @staticmethod
    def dcm_filtering(array, image_kernel = 3):
        if image_kernel % 2 == 0:
            image_kernel += 1
            
        if len(array.shape) == 2:
            array_filtered = scipy.signal.medfilt2d(array, image_kernel)
        else:
            array_filtered = np.zeros_like(array)
            for index_slice in range(array.shape[2]):
                array_filtered[:,:,index_slice] = scipy.signal.medfilt2d(array[:,:,index_slice], image_kernel)
        
        return array_filtered
    
    @staticmethod
    def arr_connected_components(array, connections = 4):
        output = cv2.connectedComponentsWithStats(array,\
                                                  connections,cv2.CV_32S)
            
        return output
    
    @staticmethod
    def dcm_threshold(array, threshold, multiplier = 1): 
        if threshold > 0:
            if len(array.shape) == 2:
                tmp_array = array
                tmp_array[tmp_array <= threshold * multiplier] = 0
                tmp_array[tmp_array >= threshold * multiplier] = 1
                    
                
            else:
                for index in range(array.shape[2]):
                    tmp_array = array[:,:,index]
                    if type(threshold) == int:
                        tmp_threshold = threshold * multiplier
                    else:
                        tmp_threshold = threshold[index] * multiplier
             
                    tmp_array[tmp_array <= tmp_threshold] = 0
                    tmp_array[tmp_array >= tmp_threshold] = 1
             
                    array[:,:,index] = tmp_array
        else:
            if len(array.shape) == 2:
                tmp_array = array
                tmp_array[tmp_array <= threshold * multiplier] = threshold*multiplier - 1
                tmp_array[tmp_array >= threshold * multiplier] = 1
                tmp_array[tmp_array < 1] = 0
                    
                
            else:
                for index in range(array.shape[2]):
                    tmp_array = array[:,:,index]
                    if type(threshold) == int:
                        tmp_threshold = threshold * multiplier
                    else:
                        tmp_threshold = threshold[index] * multiplier
             
                    tmp_array[tmp_array <= tmp_threshold] = tmp_threshold - 1
                    tmp_array[tmp_array >= tmp_threshold] = 1
                    tmp_array[tmp_array < 1] = 0
             
                    array[:,:,index] = tmp_array
            
        array = array.astype(dtype = np.uint8)
     
        return array

    @staticmethod
    def pop_noncircular_objects(array, sphere_dict, sphere_est = 0.4):
        poppable_keys = []
        
        for key in sphere_dict.keys():
            start_coordinate = [int(sphere_dict[key][0]), int(sphere_dict[key][1])]
            
            x_right = 0
            while array[start_coordinate[0], start_coordinate[1] + x_right] == 1:
                if start_coordinate[1] + x_right + 1 < array.shape[1]:
                    x_right += 1
                else:
                    break
            
            x_left = 0
            while array[start_coordinate[0], start_coordinate[1] - x_left] == 1:
                if start_coordinate[1] - x_left - 1 < 0:
                    x_left += 1
                else:
                    break
            
            y_top = 0
            while array[start_coordinate[0] + y_top, start_coordinate[1]] == 1:
                if start_coordinate[0] + y_top + 1 < array.shape[0]:
                    y_top += 1
                else:
                    break
            
            y_bottom = 0
            while array[start_coordinate[0] - y_bottom, start_coordinate[1]] == 1:
                if start_coordinate[1] - y_bottom - 1 < 0:
                    y_bottom += 1
                else:
                    break
                
            x_dist = x_right + x_left
            y_dist = y_top + y_bottom
            
            if x_dist not in range(int((1 - sphere_est)*y_dist),\
                                   int((1 + sphere_est)*y_dist)):
                poppable_keys.append(key)
            else:
                pass
            
        for key in poppable_keys:
                sphere_dict.pop(key)
                
        return sphere_dict
    
    @staticmethod
    def create_circular_mask(h, w, center_circle, radius_circle):

        Y, X = np.ogrid[:h, :w]
        dist_from_center = np.sqrt((X - center_circle[0])**2 + (Y-center_circle[1])**2)
    
        mask = dist_from_center <= radius_circle
        
        return mask
        
    
    @classmethod
    def find_circle(cls, array, threshold, physic_size, physic_size_est = 0.5, kernel = 5, sphere_est = 0.4, plot_centers = False):
        # array_ori = array.copy()
        
        array_threshold = cls.dcm_threshold(array.copy(), threshold)
        array_filtered = cls.dcm_filtering(array_threshold, kernel)
        
        output = cls.arr_connected_components(array_filtered, connections = 4)
       
        circle_centers = {}
        #add all centers if larger in range of physical area of spheres
        for index in range(1,output[0]):
            physical_size = physic_size
            measured_size = output[2][index][4]
            if measured_size in range (int(physical_size * (1 - physic_size_est)), int(physical_size * (1 + physic_size_est))):
                circle_centers[index] = [round(output[3][index][1]), round(output[3][index][0])]
                
        circle_centers = cls.pop_noncircular_objects(array_filtered, circle_centers, sphere_est)
               
        if plot_centers:
            plt.imshow(array_filtered)
            plt.show()
        
        if len(circle_centers) == 1:
            for key in circle_centers.keys():
                circle_centers = circle_centers[key]
        
        
        return circle_centers
        
    @staticmethod 
    def distance_between_points(coor1, coor2):
        x1 = coor1[0]
        x2 = coor2[0]
        y1 = coor1[1]
        y2 = coor2[1]
        
        distance = math.sqrt((x2 - x1)**2 + (y2 - y1)**2)
        
        return distance
    
    @classmethod 
    def calc_mean_SD(cls, array, center, ROI_radius):
        array = array.copy()
        mask = cls.create_circular_mask(array.shape[0], array.shape[1], center, ROI_radius)

        
        flat_mask = mask.flatten()
            
        if array.ndim == 2:
            flat_masked_array = (mask * array).flatten()
            ROI_array = flat_masked_array[flat_mask]
            
        elif array.ndim == 3:
            ROI_array = []
            for slice_index in range(array.shape[2]):
                flat_masked_array = (mask * array[:,:,slice_index]).flatten()
                ROI_array_tmp = flat_masked_array[flat_mask]
                ROI_array = np.append(ROI_array, ROI_array_tmp)
            
        else:
             raise ValueError("Calculation of 1 or >3 dimensional array is not defined")   
        
        
        mean = round(ROI_array.mean(), 2)
        SD = round(ROI_array.std(), 2)
        return mean, SD
    
    @staticmethod
    def dcm_interpolation(array, header):
        # define interpolation method via order:
        array_int = array.copy()
        int_header = copy.deepcopy(header)
        
        int_header.SliceThickness = header.PixelSpacing[0]
        
        # actual interpolation
        steps = [header.Rows + 1, header.Columns + 1, header.SliceThickness]    # original step sizes
        x, y, z = [steps[k] * np.arange(array_int.shape[k]) for k in range(3)]  # original grid
        f = rgi((x, y, z), array_int)    # interpolator
        
        dx, dy, dz = header.Rows, header.Columns,  header.PixelSpacing[0]    # new step sizes
        new_grid = np.mgrid[0:x[-1]:dx, 0:y[-1]:dy, 0:z[-1]:dz]   # new grid
        new_grid = np.moveaxis(new_grid, (0, 1, 2, 3), (3, 0, 1, 2))  # reorder axes for evaluation
        new_values = f(new_grid)
    
        return int_header, new_values
    
    
    @staticmethod    
    def interp_2D(array, factor, interp_type = 'cubic'):
        new_size = complex(array.shape[0] * factor)
        vals = np.reshape(array,(array.shape[0]*array.shape[1]))
        
        pts = np.array([[i,j] for i in range(array.shape[0]) for j in range(array.shape[1])] )
        
        grid_x, grid_y = np.mgrid[0:array.shape[0]-1:new_size, 0:array.shape[1]-1:new_size]
    
        new_array = inter.griddata(pts, vals, (grid_x, grid_y), method=interp_type)
        return new_array
    
    
    @classmethod
    def nps_calc(cls, image, pixel_size, center, num_ROIs = 15, rad = 32, ROI_size = 15, plot_NPS = False):
        # init
        ROI_image = image.copy()
        
        radius = int(rad / pixel_size)
        size_ROI = int(ROI_size / pixel_size)
        if (size_ROI % 2) == 0:  
            size_ROI += 1
    
        Spacing_X = pixel_size
        Spacing_Y = pixel_size
        num_x = size_ROI
        num_y = size_ROI
        
        FFT_image_total = np.zeros([size_ROI, size_ROI])
        
        angle = 0
        for index in range(num_ROIs - 1): #min 1 because center is added later
            new_center = [center[0] + math.sin(angle) * radius, center[1] + math.cos(angle) * radius]
            
            dist_x_1 = int(new_center[0] - size_ROI/2)
            dist_x_2 = int(new_center[0] + size_ROI/2)
            dist_y_1 = int(new_center[1] - size_ROI/2)
            dist_y_2 = int(new_center[1] - size_ROI/2)
            
            if (dist_x_2 - dist_x_1) != size_ROI:
                dist_x_2 += size_ROI - (dist_x_2 - dist_x_1)
            if (dist_y_2 - dist_y_1) != size_ROI:
                dist_y_2 += size_ROI - (dist_y_2 - dist_y_1)
            
            image2 = image[dist_x_1: dist_x_2,dist_y_1: dist_y_2]
        
            angle += math.pi * 2 / (num_ROIs - 1)
    
            ROI_image[int(new_center[0] - size_ROI/2): int(new_center[0] + size_ROI/2),\
                       int(new_center[1] - size_ROI/2): int(new_center[1] + size_ROI/2)] = 200
                       
            # 1. calculate mean of ROI and subtract from image
            NPS_ROI = image2 - image2.mean()
            
            # 2. 2D fourier trasnform
            FFT_image = np.fft.fftshift(np.fft.fft2(NPS_ROI))
            FFT_image = abs(FFT_image)**2
            FFT_image_total += FFT_image
            
        #add center ROI
        image2 = image[int(center[0] - size_ROI/2): int(center[0] + size_ROI/2),\
                           int(center[1] - size_ROI/2): int(center[1] + size_ROI/2)]
        ROI_image[int(center[0] - size_ROI/2): int(center[0] + size_ROI/2),\
                           int(center[1] - size_ROI/2): int(center[1] + size_ROI/2)] = 200
        
        NPS_ROI = image2 - image2.mean()
        
        FFT_image = np.fft.fftshift(np.fft.fft2(NPS_ROI))
        FFT_image = abs(FFT_image)**2
        FFT_image_total += FFT_image
        
        # calculate 2D NPS
        res_FFT = FFT_image_total * ((Spacing_X * Spacing_Y) / (num_x * num_y)) / num_ROIs
        factor = 3 #must be odd
        size_ROI = factor * size_ROI
        res_FFT = cls.interp_2D(res_FFT, factor)
        
        rad_angles = 360
        NPS_1D = res_FFT[int(size_ROI/2),int(size_ROI/2):] / rad_angles
        for index in range(1,rad_angles):
            tmp_FFT = res_FFT.copy()
            tmp_FFT = rotate(tmp_FFT,index)
            new_plot = tmp_FFT[int(size_ROI/2),int(size_ROI/2):] / rad_angles
            NPS_1D += new_plot
            
        #NPS_x = np.linspace(0,1,int(size_ROI/2) + 1)
        NPS_x = np.linspace(0,size_ROI*3, size_ROI*3) / (size_ROI*3 * (pixel_size/3))
        NPS_x = NPS_x[:math.ceil(size_ROI / 2)]
        NPS = [NPS_x, NPS_1D.round(3)]
        
        if plot_NPS:
            plt.imshow(ROI_image, cmap = 'bone')
            plt.show()
            plt.imshow(res_FFT, cmap = 'bone')
            plt.show()
            plt.scatter(NPS[0], NPS[1])
            plt.plot(NPS[0], NPS[1])
            plt.ylim(0,1.1*NPS[1].max())
            plt.show()
            
        return NPS    
    
    
class CAC_scoring():
    def __init__(self):
        
        pass
    
    
    @staticmethod
    def min_area_det(manufacturer, pixelspacing):
        if 'SIEMENS' == manufacturer:
            min_area = 0
        elif 'PHILIPS' == manufacturer:
            min_area = 0.5
        elif 'CANON' == manufacturer:
            min_area = 3 * pixelspacing**2 #min 3 connected voxels
        elif 'GE' == manufacturer:
            min_area = 1
        elif 'Literature' == manufacturer:
            min_area = 1
        else:
            min_area = 1
        
        return min_area
    
    @staticmethod
    def mass_mean_def(array, calcium_threshold):
        # Calculation of mean value within masked image
        if array.max() == 0:
            mass_mean = 0
        else:
            array[array < calcium_threshold] = 0
            mass_mean = array.sum() / np.count_nonzero(array)
    
        return mass_mean
    
    
    @staticmethod
    def AS_weight_calc(mask, calcium_image_slice, calcium_threshold_declaration, imType):
        maximum_voxel = (mask * calcium_image_slice).max()
        
        try:
            if calcium_threshold_declaration == 'monoE':
                if 'ME40KEV' in imType:
                    if maximum_voxel >= 1029:
                        AS_weight = 4
                    elif maximum_voxel >= 773:
                        AS_weight = 3
                    elif maximum_voxel >= 515:
                        AS_weight = 2
                    else:
                        AS_weight = 1
                elif 'ME50KEV' in imType:
                    if maximum_voxel >= 694:
                        AS_weight = 4
                    elif maximum_voxel >= 522:
                        AS_weight = 3
                    elif maximum_voxel >= 348:
                        AS_weight = 2
                    else:
                        AS_weight = 1
                elif 'ME60KEV' in imType:
                    if maximum_voxel >= 508:
                        AS_weight = 4
                    elif maximum_voxel >= 381:
                        AS_weight = 3
                    elif maximum_voxel >= 254:
                        AS_weight = 2
                    else:
                        AS_weight = 1
                elif 'ME70KEV' in imType:
                    if maximum_voxel >= 400:
                        AS_weight = 4
                    elif maximum_voxel >= 300:
                        AS_weight = 3
                    elif maximum_voxel >= 200:
                        AS_weight = 2
                    else:
                        AS_weight = 1
                elif 'ME80KEV' in imType:
                    if maximum_voxel >= 335:
                        AS_weight = 4
                    elif maximum_voxel >= 251:
                        AS_weight = 3
                    elif maximum_voxel >= 168:
                        AS_weight = 2
                    else:
                        AS_weight = 1
                elif 'ME90KEV' in imType:
                    if maximum_voxel >= 293:
                        AS_weight = 4
                    elif maximum_voxel >= 220:
                        AS_weight = 3
                    elif maximum_voxel >= 147:
                        AS_weight = 2
                    else:
                        AS_weight = 1
                elif 'ME100KEV' in imType:
                    if maximum_voxel >= 265:
                        AS_weight = 4
                    elif maximum_voxel >= 199:
                        AS_weight = 3
                    elif maximum_voxel >= 133:
                        AS_weight = 2
                    else:
                        AS_weight = 1
                elif 'ME110KEV' in imType:
                    if maximum_voxel >= 246:
                        AS_weight = 4
                    elif maximum_voxel >= 185:
                        AS_weight = 3
                    elif maximum_voxel >= 123:
                        AS_weight = 2
                    else:
                        AS_weight = 1
                elif 'ME120KEV' in imType:
                    if maximum_voxel >= 232:
                        AS_weight = 4
                    elif maximum_voxel >= 174:
                        AS_weight = 3
                    elif maximum_voxel >= 116:
                        AS_weight = 2
                    else:
                        AS_weight = 1
                elif 'ME130KEV' in imType:
                    if maximum_voxel >= 222:
                        AS_weight = 4
                    elif maximum_voxel >= 167:
                        AS_weight = 3
                    elif maximum_voxel >= 111:
                        AS_weight = 2
                    else:
                        AS_weight = 1
                elif 'ME140KEV' in imType:
                    if maximum_voxel >= 215:
                        AS_weight = 4
                    elif maximum_voxel >= 161:
                        AS_weight = 3
                    elif maximum_voxel >= 107:
                        AS_weight = 2
                    else:
                        AS_weight = 1
                elif 'ME150KEV' in imType:
                    if maximum_voxel >= 209:
                        AS_weight = 4
                    elif maximum_voxel >= 157:
                        AS_weight = 3
                    elif maximum_voxel >= 104:
                        AS_weight = 2
                    else:
                        AS_weight = 1
                elif 'ME160KEV' in imType:
                    if maximum_voxel >= 204:
                        AS_weight = 4
                    elif maximum_voxel >= 153:
                        AS_weight = 3
                    elif maximum_voxel >= 102:
                        AS_weight = 2
                    else:
                        AS_weight = 1
                elif 'ME170KEV' in imType:
                    if maximum_voxel >= 201:
                        AS_weight = 4
                    elif maximum_voxel >= 151:
                        AS_weight = 3
                    elif maximum_voxel >= 100:
                        AS_weight = 2
                    else:
                        AS_weight = 1
                elif 'ME180KEV' in imType:
                    if maximum_voxel >= 198:
                        AS_weight = 4
                    elif maximum_voxel >= 148:
                        AS_weight = 3
                    elif maximum_voxel >= 99:
                        AS_weight = 2
                    else:
                        AS_weight = 1
                elif 'ME190KEV' in imType:
                    if maximum_voxel >= 195:
                        AS_weight = 4
                    elif maximum_voxel >= 147:
                        AS_weight = 3
                    elif maximum_voxel >= 98:
                        AS_weight = 2
                    else:
                        AS_weight = 1
                else:
                    if maximum_voxel >= 400:
                        AS_weight = 4
                    elif maximum_voxel >= 300:
                        AS_weight = 3
                    elif maximum_voxel >= 200:
                        AS_weight = 2
                    else:
                        AS_weight = 1
            else:
                if maximum_voxel >= 400:
                    AS_weight = 4
                elif maximum_voxel >= 300:
                    AS_weight = 3
                elif maximum_voxel >= 200:
                    AS_weight = 2
                else:
                    AS_weight = 1
                
        except:
            if maximum_voxel >= 400:
                AS_weight = 4
            elif maximum_voxel >= 300:
                AS_weight = 3
            elif maximum_voxel >= 200:
                AS_weight = 2
            else:
                AS_weight = 1
    
        return AS_weight 
    
    @staticmethod
    def calc_mean_calculator(calc_dict_tmp, key_calc_dict, arr_mean, ML_array, slice_ind):
        arr_mean[arr_mean != slice_ind] = 0
        arr_mean[arr_mean == slice_ind] = 1
        tmp_mean_arr = arr_mean * ML_array
       
        mean_tmp = tmp_mean_arr.sum() 
        mean_count = np.count_nonzero(tmp_mean_arr)
        
        try:
            calc_dict_tmp[key_calc_dict][0] += mean_tmp
            calc_dict_tmp[key_calc_dict][1] += mean_count
                
        except:
            calc_dict_tmp[key_calc_dict] = [mean_tmp, mean_count]
    
        return calc_dict_tmp
    
    
    @staticmethod
    def SNR_dict_calculator(SNR_dict_tmp, key_SNR_dict, arr_mean, ML_array, slice_ind):
        arr_mean[arr_mean != slice_ind] = 0
        arr_mean[arr_mean == slice_ind] = 1
        tmp_mean_arr = arr_mean * ML_array
        
        SNR_dict_tmp[key_SNR_dict] = tmp_mean_arr.sum() / np.count_nonzero(tmp_mean_arr)
        
        return SNR_dict_tmp
    
    
class Excel_output():
    def __init__(self, output_path, phantom, scoring_method):
        self.init_excel_file(output_path, phantom, scoring_method)
        
        # used_data = wb.add_worksheet('Used_data')
        # IQ_data = wb.add_worksheet('IQ_data')
        # NPS_data = wb.add_worksheet('NPS')
        # MTF_data = wb.add_worksheet('MTF')
        # ESF_data = wb.add_worksheet('ESF')
        # not_used_sheet = wb.add_worksheet('Unused_data')
        # row_not_used = 0
        # row_used = 1
        # row_NPS = 1
        # row_MTF = 1
        # row_IQ = 1
        # row_ESF = 1
                
        return None    
    
    def init_excel_file(self, output_path, phantom, scoring_method):
        excel_path = os.path.join(output_path, 'Results_' + phantom + '_' + str(date.today()) + scoring_method + '.xlsx')
        print(excel_path)
        self.wb = xlsxwriter.Workbook(excel_path)
        self.used_data = self.wb.add_worksheet('Used_data')
        self.IQ_data = self.wb.add_worksheet('IQ_data')
        self.NPS_data = self.wb.add_worksheet('NPS')
        self.MTF_data = self.wb.add_worksheet('MTF')
        self.ESF_data = self.wb.add_worksheet('ESF')
        self.not_used_sheet = self.wb.add_worksheet('Unused_data')
        self.row_not_used = 0
        self.row_used = 1
        self.row_NPS = 1
        self.row_MTF = 1
        self.row_IQ = 1
        self.row_ESF = 1
        
        return None
    
        
    def write_CCI_to_excel(self, data, dcm_name):
        column = 0
        self.used_data.write(0, 0, 'Phantom')
        self.used_data.write(0, 1, 'Scan_description')
        self.used_data.write(0, 2, 'Calcification')
        self.used_data.write(0, 3, 'Volume score')
        self.used_data.write(0, 4, 'Agatston score')
        self.used_data.write(0, 5, 'Mass score')
        self.used_data.write(0, 6, 'Mass calibration factor')
        self.used_data.write(0, 7, 'mAs')
        self.used_data.write(0, 8, 'kVp')
        self.used_data.write(0, 9, 'SliceThickness')
        self.used_data.write(0, 10, 'SliceIncrement')
        self.used_data.write(0, 11, 'CTDIvol')
        self.used_data.write(0, 12, 'Accessionnr')
        self.used_data.write(0, 13, 'Path')

        scan_name = dcm_name.rsplit('\\')[len(dcm_name.rsplit('\\'))-1]
        if 'CCI_Large' in dcm_name:
            phantom = 'large'
        elif 'CCI_small' in dcm_name:
            phantom = 'small'
        else:
            phantom = 'unknown'
        
        if data.header.ManufacturerModelName == 'PCCT':
            exposure = data.header.XRayTubeCurrent
        else:
            exposure = data.header.Exposure
            
            
        scan_name = str(data.header.SeriesDescription) + '_' + str(data.header.Exposure)
        # recon_mode = data.header[0x01F1,0x1002].value #Focal spot size IQon
        # if ('STD' in recon_mode) and ('AIDR' not in recon_mode):
            # recon_mode = data.header[0x7005,0x1092].value.decode("utf-8") 
        # else:
        #     pass
        scan_name = str(data.header.SeriesDescription) + str(data.header.ConvolutionKernel)
        scan_name = str(data.header.SeriesDescription)
        
        for key in data.calc_size_density_VS_AS_MS.keys():
            self.used_data.write(self.row_used, column, phantom)
            self.used_data.write(self.row_used, column + 1, scan_name)
            self.used_data.write(self.row_used, column + 2, key)
            self.used_data.write(self.row_used, column + 3, data.calc_size_density_VS_AS_MS[key][1])
            self.used_data.write(self.row_used, column + 4, data.calc_size_density_VS_AS_MS[key][2])
            self.used_data.write(self.row_used, column + 5, data.calc_size_density_VS_AS_MS[key][3])
            self.used_data.write(self.row_used, column + 6, data.mass_cal_factor * 1000)
            try:
                self.used_data.write(self.row_used, column + 7, exposure) #data.header.XRayTubeCurrent / data.header.Exposure
            except:
                pass
            self.used_data.write(self.row_used, column + 8, data.header.KVP)
            self.used_data.write(self.row_used, column + 9, data.slice_thick_ori)
            try:
                self.used_data.write(self.row_used, column + 10, data.header.SpacingBetweenSlices)
            except:
                try:
                    self.used_data.write(self.row_used, column + 10, data.header.SliceThickness)
                except:
                    pass
            # self.used_data.write(row_used, column + 11, data.header.CTDIvol)
            # self.used_data.write(row_used, column + 12, data.header.AccessionNumber) 
            self.used_data.write(self.row_used, column + 13, dcm_name)
    
            self.row_used += 1
            
        self.IQ_data.write(0, 0, 'Phantom')
        self.IQ_data.write(0, 1, 'Scan_description')
        self.IQ_data.write(0, 2, 'Background mean HU')
        self.IQ_data.write(0, 3, 'Background SD')
        self.IQ_data.write(0, 4, 'Background AS')
        self.IQ_data.write(0, 5, 'Water_rod mean HU')
        self.IQ_data.write(0, 6, 'Water_rod SD')
        self.IQ_data.write(0, 7, 'MTF_0.5')
        self.IQ_data.write(0, 8, 'SNR High density')
        self.IQ_data.write(0, 9, 'SNR Medium density')
        self.IQ_data.write(0, 10, 'SNR Low density')
        self.IQ_data.write(0, 11, 'CNR High density')
        self.IQ_data.write(0, 12, 'CNR Medium density')
        self.IQ_data.write(0, 13, 'CNR Low density')
        self.IQ_data.write(0, 14, 'NTD')
        self.IQ_data.write(0, 15, 'NPS peak')
        self.IQ_data.write(0, 16, 'Mean_L_HD')
        self.IQ_data.write(0, 17, 'Mean_L_MD')
        self.IQ_data.write(0, 18, 'Mean_L_LD')
        self.IQ_data.write(0, 19, 'Mean_M_HD')
        self.IQ_data.write(0, 20, 'Mean_M_MD')
        self.IQ_data.write(0, 21, 'Mean_M_LD')
        self.IQ_data.write(0, 22, 'Mean_S_HD')
        self.IQ_data.write(0, 23, 'Mean_S_MD')
        self.IQ_data.write(0, 24, 'Mean_S_LD')
        self.IQ_data.write(0, 25, 'Recon')
        self.IQ_data.write(0, 26, 'ESF equation')
        self.IQ_data.write(0, 27, 'LSF equation')
        self.IQ_data.write(0, 28, 'LSF FWHM [mm]')
        
        
        self.IQ_data.write(self.row_IQ, column, phantom)
        self.IQ_data.write(self.row_IQ, column + 1, scan_name)
        self.IQ_data.write(self.row_IQ, column + 2, data.qc['mean'])
        self.IQ_data.write(self.row_IQ, column + 3, data.qc['SD'])
        self.IQ_data.write(self.row_IQ, column + 4, data.qc['AS'])
        self.IQ_data.write(self.row_IQ, column + 5, data.water_rod_metrics[0])
        self.IQ_data.write(self.row_IQ, column + 6, data.water_rod_metrics[1])
        try:
            self.IQ_data.write(self.row_IQ, column + 7, data.qc['MTF0.5'])
            self.IQ_data.write(self.row_IQ, column + 8, data.qc['SNR_HD'])
            self.IQ_data.write(self.row_IQ, column + 9, data.qc['SNR_MD'])
            self.IQ_data.write(self.row_IQ, column + 10, data.qc['SNR_LD'])
        except:
            pass
        try:
            self.IQ_data.write(self.row_IQ, column + 11, data.qc['CNR_HD'])
            self.IQ_data.write(self.row_IQ, column + 12, data.qc['CNR_MD'])
            self.IQ_data.write(self.row_IQ, column + 13, data.qc['CNR_LD'])
        except:
            pass
        self.IQ_data.write(self.row_IQ, column + 14, data.qc['NTD'])
        self.IQ_data.write(self.row_IQ, column + 15, data.qc['NPS_peak'])
        try:
            self.IQ_data.write(self.row_IQ, column + 16, data.calc_mean_dict['Large_HD'])
            self.IQ_data.write(self.row_IQ, column + 17, data.calc_mean_dict['Large_MD'])
            self.IQ_data.write(self.row_IQ, column + 18, data.calc_mean_dict['Large_LD'])
            self.IQ_data.write(self.row_IQ, column + 19, data.calc_mean_dict['Medium_HD'])
            self.IQ_data.write(self.row_IQ, column + 20, data.calc_mean_dict['Medium_MD'])
            self.IQ_data.write(self.row_IQ, column + 21, data.calc_mean_dict['Medium_LD'])
            self.IQ_data.write(self.row_IQ, column + 22, data.calc_mean_dict['Small_HD'])
            self.IQ_data.write(self.row_IQ, column + 23, data.calc_mean_dict['Small_MD'])
            self.IQ_data.write(self.row_IQ, column + 24, data.calc_mean_dict['Small_LD'])
        except:
            pass
        #self.IQ_data.write(row_IQ, column + 25, recon_mode)
        try:
            self.IQ_data.write(self.row_IQ, column + 26, data.qc['ESF_eq'])
            self.IQ_data.write(self.row_IQ, column + 27, data.qc['LSF_eq'])
            self.IQ_data.write(self.row_IQ, column + 28, data.qc['LSF_FWHM'])
        except:
            pass        
        self.row_IQ += 1
        
        self.NPS_data.write(0, 0, 'Scan_description')
        self.NPS_data.write(0, 1, 'X / Y')
        self.NPS_data.write(0, 2, 'NPS_data')
        
        try:
            NPS_x_index = 2
            for element in data.qc['NPS'][0]:
                if NPS_x_index == 2:
                    self.NPS_data.write(self.row_NPS, 0, scan_name)
                    self.NPS_data.write(self.row_NPS, 1, 'x-axis')
                self.NPS_data.write(self.row_NPS, NPS_x_index, element)
                NPS_x_index += 1
            self.row_NPS += 1    
            
            NPS_y_index = 2
            for element in data.qc['NPS'][1]:
                if NPS_y_index == 2:
                    self.NPS_data.write(self.row_NPS, 0, scan_name)
                    self.NPS_data.write(self.row_NPS, 1, 'y-axis')
                self.NPS_data.write(self.row_NPS, NPS_y_index, element)
                NPS_y_index += 1
            self.row_NPS += 1 
            
        except:
            pass
        
        try:
            self.MTF_data.write(0, 0, 'Scan_description')
            self.MTF_data.write(0, 1, 'X / Y')
            self.MTF_data.write(0, 2, 'MTF_data')
            
            MTF_x_index = 2
            for element in data.qc['MTF'][0]:
                if MTF_x_index == 2:
                    self.MTF_data.write(self.row_MTF, 0, scan_name)
                    self.MTF_data.write(self.row_MTF, 1, 'x-axis')
                self.MTF_data.write(self.row_MTF, MTF_x_index, element)
                MTF_x_index += 1
            self.row_MTF += 1    
            
            MTF_y_index = 2
            for element in data.qc['MTF'][1]:
                if MTF_y_index == 2:
                    self.MTF_data.write(self.row_MTF, 0, scan_name)
                    self.MTF_data.write(self.row_MTF, 1, 'y-axis')
                self.MTF_data.write(self.row_MTF, MTF_y_index, element)
                MTF_y_index += 1
            self.row_MTF += 1
            
        except:
            pass            
        
        try:
            self.ESF_data.write(0, 0, 'Scan_description')
            self.ESF_data.write(0, 1, 'X / Y')
            self.ESF_data.write(0, 2, 'ESF_data')
            
            ESF_x_index = 2
            x_val = 0
            for element in data.qc['ESF']:
                if ESF_x_index == 2:
                    self.ESF_data.write(self.row_ESF, 0, scan_name)
                    self.ESF_data.write(self.row_ESF, 1, 'x-axis')
                self.ESF_data.write(self.row_ESF, ESF_x_index, x_val)
                ESF_x_index += 1
                x_val += 1
            self.row_ESF += 1    
            
            ESF_y_index = 2
            for element in data.qc['ESF']:
                if ESF_y_index == 2:
                    self.ESF_data.write(self.row_ESF, 0, scan_name)
                    self.ESF_data.write(self.row_ESF, 1, 'y-axis')
                self.ESF_data.write(self.row_ESF, ESF_y_index, element)
                ESF_y_index += 1
            self.row_ESF += 1
        
        except:
            pass
    
        return None
    
    
    def write_SolidArtery_to_excel(self, data, dcm_name):
        column = 0
        self.used_data.write(0, 0, 'Scan_description')
        self.used_data.write(0, 1, 'AS_1')
        self.used_data.write(0, 2, 'MS_1')
        self.used_data.write(0, 3, 'VS_1')
        self.used_data.write(0, 4, 'AS_2')
        self.used_data.write(0, 5, 'MS_2')
        self.used_data.write(0, 6, 'VS_2')
        self.used_data.write(0, 7, 'Mass calibration factor')
        self.used_data.write(0, 8, 'mAs')
        self.used_data.write(0, 9, 'kVp')
        self.used_data.write(0, 10, 'Time')
        self.used_data.write(0, 11, 'Recon Kernel')
        self.used_data.write(0, 12, 'CTDIvol')
        self.used_data.write(0, 13, 'Threshold')
        self.used_data.write(0, 14, 'Accnr')
        self.used_data.write(0, 15, 'SliceThickness')
        self.used_data.write(0, 16, 'Increment')
        self.used_data.write(0, 17, 'DCM_name')
        
        
        scan_name = dcm_name.rsplit('\\')[len(dcm_name.rsplit('\\'))-1]
        
        if 'Large' in dcm_name:
            phantom = 'large'
        elif 'small' in dcm_name:
            phantom = 'small'
        else:
            phantom = 'unknown'
        
        
        
        self.used_data.write(self.row_used, column, scan_name)
        self.used_data.write(self.row_used, column + 1, data.scores_calc1[0])
        self.used_data.write(self.row_used, column + 2, data.scores_calc1[1])
        self.used_data.write(self.row_used, column + 3, data.scores_calc1[2])
        self.used_data.write(self.row_used, column + 4, data.scores_calc2[0])
        self.used_data.write(self.row_used, column + 5, data.scores_calc2[1])
        self.used_data.write(self.row_used, column + 6, data.scores_calc2[2])
        self.used_data.write(self.row_used, column + 7, data.mass_cal_factor * 1000)
        self.used_data.write(self.row_used, column + 8, data.header.XRayTubeCurrent)
        self.used_data.write(self.row_used, column + 9, data.header.KVP)
        self.used_data.write(self.row_used, column + 10, data.header.AcquisitionTime)
        try:
            self.used_data.write(self.row_used, column + 11, data.header.ConvolutionKernel)
        except:
            self.used_data.write(self.row_used, column + 11, data.header.ConvolutionKernel[0])
        self.used_data.write(self.row_used, column + 12, data.header.CTDIvol)
        self.used_data.write(self.row_used, column + 13, data.header.SeriesTime)
        self.used_data.write(self.row_used, column + 14, data.header.AccessionNumber)
        self.used_data.write(self.row_used, column + 15, data.slice_thick_ori)
        try:
            self.used_data.write(self.row_used, column + 16, data.header.SpacingBetweenSlices)
        except:
            try:
                self.used_data.write(self.row_used, column + 16, data.header.SliceThickness)
            except:
                pass
        self.used_data.write(self.row_used, column + 17, dcm_name)
        try:
            self.used_data.write(self.row_used, column + 14, str(data.header[0x7005,0x100b].value))
            if str(data.header[0x7005,0x100b].value) == 'ORG':
                self.used_data.write(self.row_used, column + 14, "FBP")    
            if str(data.header[0x7005,0x1092].value) != "volumeXact+":
                self.used_data.write(self.row_used, column + 14, str(data.header[0x7005,0x1092].value))
        except: 
            pass
    
            
    
        self.row_used += 1
        
        self.IQ_data.write(0, 0, 'Phantom')
        self.IQ_data.write(0, 1, 'Scan_description')
        self.IQ_data.write(0, 2, 'Background mean HU')
        self.IQ_data.write(0, 3, 'Background SD')
        self.IQ_data.write(0, 4, 'Background AS')
        self.IQ_data.write(0, 5, 'NTD')
        self.IQ_data.write(0, 6, 'NPS peak')
        self.IQ_data.write(0, 7, 'Mean_calc1')
        self.IQ_data.write(0, 8, 'SNR_calc1')
        self.IQ_data.write(0, 9, 'CNR_calc1')
        self.IQ_data.write(0, 10, 'Number_voxels_calc1')
        self.IQ_data.write(0, 11, 'Max_calc1')
        self.IQ_data.write(0, 12, 'Mean_calc2')
        self.IQ_data.write(0, 13, 'SNR_calc2')
        self.IQ_data.write(0, 14, 'CNR_calc2')
        self.IQ_data.write(0, 15, 'Number_voxels_calc2')
        self.IQ_data.write(0, 16, 'Max_calc2')
        
        self.IQ_data.write(self.row_IQ, column, phantom)
        self.IQ_data.write(self.row_IQ, column + 1, scan_name)
        self.IQ_data.write(self.row_IQ, column + 2, data.qc['mean'])
        self.IQ_data.write(self.row_IQ, column + 3, data.qc['SD'])
        self.IQ_data.write(self.row_IQ, column + 4, data.qc['AS'])
        #self.IQ_data.write(self.row_IQ, column + 5, data.qc['NTD'])
        #self.IQ_data.write(self.row_IQ, column + 6, data.qc['NPS_peak'])
        self.IQ_data.write(self.row_IQ, column + 7, data.qc['mean_calc1'])
        self.IQ_data.write(self.row_IQ, column + 8, data.qc['SNR_calc1'])
        self.IQ_data.write(self.row_IQ, column + 9, data.qc['CNR_calc1'])
        self.IQ_data.write(self.row_IQ, column + 10, data.qc['num_vox_calc1'])
        self.IQ_data.write(self.row_IQ, column + 11, data.max_calc1)#header.SliceThickness * (calc1[1] - calc1[0]))
        self.IQ_data.write(self.row_IQ, column + 12, data.qc['mean_calc2'])
        self.IQ_data.write(self.row_IQ, column + 13, data.qc['SNR_calc2'])
        self.IQ_data.write(self.row_IQ, column + 14, data.qc['CNR_calc2'])
        self.IQ_data.write(self.row_IQ, column + 15, data.qc['num_vox_calc2'])
        self.IQ_data.write(self.row_IQ, column + 16, data.max_calc2) #header.SliceThickness * (calc2[1] - calc2[0]))
        
        self.row_IQ += 1
        

        
        return None
    
    def write_HollowArtery_to_excel(self, data, dcm_name, Artery_blooming_calc):
        row_used = self.row_used
        column = 0
        
        self.used_data.write(0, 0, 'Scan_description')
        self.used_data.write(0, 1, 'CAC_density')
        self.used_data.write(0, 2, 'AS')
        self.used_data.write(0, 3, 'MS')
        self.used_data.write(0, 4, 'VS')
        self.used_data.write(0, 5, 'Mean_calc')
        self.used_data.write(0, 6, 'Num_vox')
        self.used_data.write(0, 7, 'Max_calc')
        self.used_data.write(0, 8, 'CNR_calc')
        self.used_data.write(0, 9, 'Mean_background')
        self.used_data.write(0, 10, 'SD_background')
        self.used_data.write(0, 11, 'AS_background')
        self.used_data.write(0, 12, 'AcquisitionTime')
        self.used_data.write(0, 13, 'CTDI')
        self.used_data.write(0, 14, 'path')
        
        scan_name = dcm_name.rsplit('\\')[len(dcm_name.rsplit('\\'))-1]
        
        for key in data.CAC_scores.keys():
            self.used_data.write(row_used, column, scan_name)
            self.used_data.write(row_used, column + 1, key)
            self.used_data.write(row_used, column + 2, data.CAC_scores[key][0])
            self.used_data.write(row_used, column + 3, data.CAC_scores[key][1])
            self.used_data.write(row_used, column + 4, data.CAC_scores[key][2])
            self.used_data.write(row_used, column + 5, data.CAC_scores[key][3])
            self.used_data.write(row_used, column + 6, data.CAC_scores[key][4])
            self.used_data.write(row_used, column + 7, data.CAC_scores[key][5])
            self.used_data.write(row_used, column + 8, round((data.CAC_scores[key][3] - data.qc['mean']) / data.qc['SD'],2))
            self.used_data.write(row_used, column + 9, data.qc['mean'])
            self.used_data.write(row_used, column + 10, data.qc['SD'])
            self.used_data.write(row_used, column + 11, data.qc['AS'])
            self.used_data.write(row_used, column + 12, str(data.header.AcquisitionTime))
            try:
                self.used_data.write(row_used, column + 13, str(round(data.header.CTDIvol,2)))
            except:
                pass
            self.used_data.write(row_used, column + 14, dcm_name)
            
            
            row_used += 1
            
        self.row_used = row_used
            
        
        if Artery_blooming_calc:
            self.IQ_data.write(0, 0, 'Scan_description')
            self.IQ_data.write(0, 1, 'LD_water_mean')
            self.IQ_data.write(0, 2, 'LD_CAC800_mean')
            self.IQ_data.write(0, 3, 'LD_CAC400_mean')
            self.IQ_data.write(0, 4, 'LD_CAC200_mean')
            self.IQ_data.write(0, 5, 'LD_CAC100_mean')
            self.IQ_data.write(0, 6, 'LD_CAC75_mean')
            self.IQ_data.write(0, 7, 'LD_water_SD')
            self.IQ_data.write(0, 8, 'LD_CAC800_SD')
            self.IQ_data.write(0, 9, 'LD_CAC400_SD')
            self.IQ_data.write(0, 10, 'LD_CAC200_SD')
            self.IQ_data.write(0, 11, 'LD_CAC100_SD')
            self.IQ_data.write(0, 12, 'LD_CAC75_SD')
            self.IQ_data.write(0, 13, 'RD_water')
            self.IQ_data.write(0, 14, 'RD_CAC800')
            self.IQ_data.write(0, 15, 'RD_CAC400')
            self.IQ_data.write(0, 16, 'RD_CAC200')
            self.IQ_data.write(0, 17, 'RD_CAC100')
            self.IQ_data.write(0, 18, 'RD_CAC75')
            
            self.IQ_data.write(self.row_IQ, column, scan_name)
            self.IQ_data.write(self.row_IQ, column + 1, data.lumen_diameter['water'][0])
            self.IQ_data.write(self.row_IQ, column + 2, data.lumen_diameter['CAC_800'][0])
            self.IQ_data.write(self.row_IQ, column + 3, data.lumen_diameter['CAC_400'][0])
            self.IQ_data.write(self.row_IQ, column + 4, data.lumen_diameter['CAC_200'][0])
            self.IQ_data.write(self.row_IQ, column + 5, data.lumen_diameter['CAC_100'][0])
            self.IQ_data.write(self.row_IQ, column + 6, data.lumen_diameter['CAC_75'][0])
            self.IQ_data.write(self.row_IQ, column + 7, data.lumen_diameter['water'][1])
            self.IQ_data.write(self.row_IQ, column + 8, data.lumen_diameter['CAC_800'][1])
            self.IQ_data.write(self.row_IQ, column + 9, data.lumen_diameter['CAC_400'][1])
            self.IQ_data.write(self.row_IQ, column + 10, data.lumen_diameter['CAC_200'][1])
            self.IQ_data.write(self.row_IQ, column + 11, data.lumen_diameter['CAC_100'][1])
            self.IQ_data.write(self.row_IQ, column + 12, data.lumen_diameter['CAC_75'][1])
            self.IQ_data.write(self.row_IQ, column + 13, data.rel_diameter['water'])
            self.IQ_data.write(self.row_IQ, column + 14, data.rel_diameter['CAC_800'])
            self.IQ_data.write(self.row_IQ, column + 15, data.rel_diameter['CAC_400'])
            self.IQ_data.write(self.row_IQ, column + 16, data.rel_diameter['CAC_200'])
            self.IQ_data.write(self.row_IQ, column + 17, data.rel_diameter['CAC_100'])
            self.IQ_data.write(self.row_IQ, column + 18, data.rel_diameter['CAC_75'])

        
            self.row_IQ += 1
        return None
    
