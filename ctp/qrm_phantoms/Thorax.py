# main document analysis of QRM thorax

import numpy as np
import scipy
import math
from ctp.Phantom_analysis_toolbox import Phantom_analysis_toolbox as pat
from ctp.qrm_phantoms import constants as C
import os
from sys import platform 
import pydicom

    
class QRM_thorax_analyser(pat):
    def __init__(self, used_path):
        super().__init__(used_path)
        self.calcium_threshold = self.threshold_def(self.header,\
                                                    C.calcium_threshold)
        self.scoring_method = self.scoring_method_def(self.header.Manufacturer,\
                                               C.scoring_method)
            
        self.dcm_masked(array = self.array.copy(),\
                        center_slice = int(self.array.shape[2]/2))
        
        self.WAD_output = {}       

        return None
    
    @staticmethod
    def threshold_def(header, threshold_declaration):
        if threshold_declaration == 'default':
            if header.KVP == 120:
                return 130
            elif header.KVP == 100:
                return 147
            else:
                return 130
        elif threshold_declaration == 'monoE':
            if 'ME40KEV' in header.ImageType:
                return 339
            elif 'ME50KEV' in header.ImageType:
                return 229
            elif 'ME60KEV' in header.ImageType:
                return 167
            elif 'ME70KEV' in header.ImageType:
                return 130
            elif 'ME80KEV' in header.ImageType:
                return 110
            elif 'ME90KEV' in header.ImageType:
                return 97
            elif 'ME100KEV' in header.ImageType:
                return 87
            elif 'ME110KEV' in header.ImageType:
                return 81
            elif 'ME120KEV' in header.ImageType:
                return 77
            elif 'ME130KEV' in header.ImageType:
                return 73
            elif 'ME140KEV' in header.ImageType:
                return 71
            elif 'ME150KEV' in header.ImageType:
                return 69
            elif 'ME160KEV' in header.ImageType:
                return 67
            elif 'ME170KEV' in header.ImageType:
                return 66
            elif 'ME180KEV' in header.ImageType:
                return 65
            elif 'ME190KEV' in header.ImageType:
                return 64
            else:
                return 130
            
    @staticmethod
    def scoring_method_def(manufacturer, scoring_method):
        if scoring_method == 'default':
            if ('TOSHIBA' in manufacturer or 'Toshiba' in manufacturer):
                return 'CANON'
            elif ('CANON' in manufacturer or 'Canon' in manufacturer):
                return 'CANON'
            elif ('SIEMENS' in manufacturer or 'Siemens' in manufacturer):
                return 'SIEMENS'
            elif ('PHILIPS' in manufacturer or 'Philips' in manufacturer):
                return 'Philips'
            elif ('GE' in manufacturer or 'Ge' in manufacturer):
                return 'GE'       
            else:
                print('Manufacturer unknown, consider specifying')
                return manufacturer.upper()
            
        elif (scoring_method == 'Siemens' or scoring_method == 'Siemens'):
            return 'SIEMENS'
        elif (scoring_method == 'Canon' or scoring_method == 'CANON'):
            return 'CANON'
        elif (scoring_method == 'Toshiba' or scoring_method == 'TOSHIBA'):
            return 'CANON'
        elif (scoring_method == 'Philips' or scoring_method == 'PHILIPS'):
            return 'PHILIPS'
        elif (scoring_method == 'Ge' or scoring_method == 'GE'):
            return 'GE'
        elif (scoring_method == 'literature'):
            return 'Literature'
        else:
            print('Error, scoring method unknown, using default')
            return manufacturer.upper()
        
    @staticmethod 
    def findCircle(point_1, point_2, point_3) : 
        x1, y1 = point_1
        x2, y2 = point_2
        x3, y3 = point_3
        
        x12 = x1 - x2 
        x13 = x1 - x3  
        y12 = y1 - y2  
        y13 = y1 - y3 
        y31 = y3 - y1  
        y21 = y2 - y1
        x31 = x3 - x1  
        x21 = x2 - x1 
     
        sx13 = x1**2 - x3**2  
        sy13 = y1**2 - y3**2
        sx21 = x2**2 - x1**2  
        sy21 = y2**2 - y1**2  
      
        f = (((sx13) * (x12) + (sy13) * (x12) + (sx21) * (x13) +\
              (sy21) * (x13)) // (2 * ((y31) * (x12) - (y21) * (x13)))) 
                  
        g = (((sx13) * (y12) + (sy13) * (y12) + (sx21) * (y13) + (sy21) *\
              (y13)) // (2 * ((x31) * (y12) - (x21) * (y13))))  
      
        # eqn of circle be x^2 + y^2 + 2*g*x + 2*f*y + c = 0 where centre is (h = -g, k = -f)  
        center_insert = [-g,-f]
    
        return center_insert
        
        
    def dcm_masked(self, array = None, radius_val = 95,\
                   center_slice = None):
        pixel_size = self.pixel_size
        
        radius = (radius_val/2) / pixel_size
        
        central_image = array[:,:,center_slice].copy()
        
        central_image[central_image > -200] = 0
        central_image[central_image != 0] = 1
    
        image_kernel = math.ceil(5 / pixel_size)
        if image_kernel % 2 == 0:
            image_kernel += 1
        central_image = scipy.signal.medfilt2d(central_image, image_kernel)
        
        # plt.imshow(central_image)
        # plt.show()
    
        center = [int(array.shape[0] / 2), int(array.shape[1] / 2)]
        
        a = central_image.copy()
        for index in range(int(array.shape[1] / 2)):
            if (central_image[center[0] + index, center[1] + index] == 1 and\
                central_image[center[0] + index, center[1] + index + 5] == 1):
                point_1 = [center[0] + index, center[1] + index]
                break
            else:
                a[center[0] + index, center[1] + index] = 2
                pass
        
        for index in range(int(array.shape[1] / 2)):
            if (central_image[center[0] + index, center[1] - index] == 1 and\
                central_image[center[0] + index, center[1] - index - 5] == 1):
                point_2 = [center[0] + index, center[1] - index]
                break
            else:
                a[center[0] + index, center[1] - index] = 2
                pass
            
        for index in range(int(array.shape[1] / 2)):
            if (central_image[center[0] - index, center[1] - index] == 1 and\
                central_image[center[0] - index, center[1] - index - 5] == 1):
                point_3 = [center[0] - index, center[1] - index]
                break
            else:
                a[center[0] - index, center[1] - index] = 2
                pass

        center_insert = self.findCircle(point_1, point_2, point_3)
        
        Y, X = np.ogrid[:self.header.Rows, :self.header.Columns]
        dist_from_center = np.sqrt((X - center_insert[1])**2 + (Y-center_insert[0])**2)
    
        mask = dist_from_center <= radius  
        masked_array = np.zeros_like(array)
        for index in range(array.shape[2]):
            masked_array[:,:,index] = array[:,:,index] * mask
            
        self.array_ori = self.array
        self.array = masked_array
        self.center_insert = center_insert
        self.mask = mask
    
        return None

def dcm_list_builder(path, test_text = ""):
    # function to get list of dcm_files from dcm directory
    dcm_path_list = []
    for (dirpath, dirnames, filenames) in os.walk(path,topdown=True):
        if dirpath not in dcm_path_list:
            for filename in filenames:
                try:
                    if 'win' in platform:
                        tmp_str = str('\\\\?\\' + os.path.join(dirpath, filename))
                    else:
                        tmp_str = os.path.join(dirpath, filename)
                    pydicom.read_file(tmp_str, stop_before_pixels = True)
                    if dirpath not in dcm_path_list:
                        dcm_path_list.append(dirpath)
                except:
                    pass
            else:
                pass
    return dcm_path_list          
    
   