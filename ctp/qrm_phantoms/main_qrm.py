from ctp.qrm_phantoms import Thorax
from ctp.qrm_phantoms import CCI
from ctp.qrm_phantoms import Solid_Arteries
from ctp.qrm_phantoms import Hollow_Artery
from ctp.qrm_phantoms import constants as C
from ctp.Phantom_analysis_toolbox import Excel_output as excel
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
np.seterr(all='ignore')



def main(data_output):
    dcm_name = dcm_path_list[idx]
    
    if C.phantom == 'CCI':
        output = CCI.CCI_analyser(dcm_name)
        if C.print_to_excel == True:
            data_output = excel.write_CCI_to_excel(data_output, output, dcm_name)
        
    elif C.phantom == 'solid_artery':
        output = Solid_Arteries.Solid_Artery_analyser(dcm_name)
        if C.print_to_excel == True:
            data_output = excel.write_SolidArtery_to_excel(data_output, output, dcm_name)
            
            
    elif C.phantom == 'hollow_artery':
        try:
            cac_centers = dcm.center_CAC
            output = Hollow_Artery.Hollow_Artery_analyser(dcm_name, cac_centers)
        except:
            output = Hollow_Artery.Hollow_Artery_analyser(dcm_name)
       
        if C.print_to_excel == True:
            data_output = excel.write_HollowArtery_to_excel(data_output, output, dcm_name, C.Artery_blooming_calc)
            
        
    else:
        print('Invalid phantom choice')

    return output
    
    
if __name__ == '__main__':
    print('\nUsed phantom = ', C.QRM_phantom, "with", C.phantom)
    print('Used scoring method = ', C.scoring_method)
    print('Used threshold = ', C.calcium_threshold)
    
    dcm_path_list = Thorax.dcm_list_builder(C.root_path)

    data_output = excel(C.output_path, C.phantom, C.scoring_method)
    
    for idx in tqdm(range(len(dcm_path_list))):
        # try:
        if True:
            dcm = main(data_output)
        # except:
            # data_output.not_used_sheet.write(data_output.row_not_used, 0, dcm_path_list[idx])
            # data_output.row_not_used += 1
        
    data_output.wb.close()