# main document for solid_artery analysis

# import pydicom
# import SimpleITK as sitk
# import os
import numpy as np
import scipy
import matplotlib.pyplot as plt
import cv2
import math
import statistics
from ctp.Phantom_analysis_toolbox import Phantom_analysis_toolbox as pat
from ctp.Phantom_analysis_toolbox import CAC_scoring as cac
from ctp.qrm_phantoms.Thorax import QRM_thorax_analyser as thorax
from ctp.qrm_phantoms import constants as C
from skimage.measure import profile_line
# from scipy import optimize
from scipy.signal import find_peaks
# from skimage.transform import rotate
# from scipy import optimize
# import sympy
# from sys import platform
  


class Hollow_Artery_analyser(thorax):
    def __init__(self, used_path, centers_CAC = 0):
        super().__init__(used_path)
        
        self.int_factor = self.header.SliceThickness / self.pixel_size
        
        self.arteries_masked()
        self.calcium_image_HA()
        
        if (C.Volume_score_calc and ('SIEMENS' in self.header.Manufacturer.upper() or self.header.Manufacturer == 'Literature')):
            self.int_header, self.int_array = pat.dcm_interpolation(self.array, self.header)
            
        self.mass_cal_factor = 0.001
        
        if C.Artery_blooming_calc:
            self.center_CAC = centers_CAC
            if C.Artery_blooming_calc_trigger in used_path:
                self.calc_center_CAC(plot_centers = False)
            self.FWHM()
            
        self.mask_scoring_def() 
        self.hollow_artery_scoring()
        self.arteries_quality()
        
        if C.motion_artefact_calc:
            self.motion_artefact = self.motion_character()

        
        
        return None
    
    
    def arteries_masked(self, radius_val = 80):
        radius = (radius_val/2) / self.pixel_size
        
        Y, X = np.ogrid[:self.header.Rows, :self.header.Columns]
        dist_from_center = np.sqrt((X - self.center_insert[1])**2 + (Y- self.center_insert[0])**2)
    
        mask = dist_from_center <= radius  
        masked_array = np.zeros_like(self.array)
        for index in range(self.array.shape[2]):
            masked_array[:,:,index] = self.array[:,:,index] * mask
            # plt.imshow(masked_array[:,:,index])
            # plt.show()
            
        self.array = masked_array
        self.mask = mask
    
        return None
    
    
    @staticmethod
    def find_CAC_in_HA_artery(dcm_array, fact, slice_dict, header, threshold):
        image_kernel = 3
        while (fact > 1 and len(slice_dict) == 0):
            
            artery_num_pixels = math.ceil(math.pi * (11/2)**2 / header.PixelSpacing[0]**2)
            
            while (image_kernel < 9 and len(slice_dict) == 0):
                # print(image_kernel, fact, fact*threshold)
                array = dcm_array.copy()
                array[array < fact*threshold] = 0
                array[array > 0] = 1
                array = array.astype(dtype = np.uint8)
            
                used_range = range(math.ceil(8/header.SliceThickness), array.shape[2] - math.ceil(8 / header.SliceThickness))
                # print(used_range)
                for idx in used_range:
                    array_filtered = scipy.signal.medfilt2d(array[:,:,idx], image_kernel)
                    # plt.imshow(array_filtered)
                    # plt.show()
                    
                    output = cv2.connectedComponentsWithStats(array_filtered, 4, cv2.CV_32S)
                    count_11mm = 0
                    count_large = 0
                    count_small = 0
                        
                    for index in range(1,output[0]):
                        tmp_size_calc = output[2][index][4]
                        if tmp_size_calc < artery_num_pixels * 0.3:
                            count_small += 1
                        elif tmp_size_calc > artery_num_pixels * 4:
                            count_large += 1
                        else:
                            count_11mm += 1
                            
                    # print(idx, count_small, count_large, count_11mm, dcm_array[:,:,idx].max())
                    
                    if (count_11mm == 1 and count_large == 0 and count_small < 3):
                        slice_dict[idx] = count_11mm
                        # plt.imshow(array_filtered)
                        # plt.title(idx)
                        # plt.show()
                image_kernel += 2
                fact -= 0.1
            # print(slice_dict)
            
        threshold_kernel_central = [fact * threshold, image_kernel]
        
        CAC_loc = []
        for index in range(dcm_array.shape[2]):
            if index in slice_dict.keys():
                CAC_loc.append(index)
            else:
                pass
            
        CAC_800_loc = []
        while len(CAC_loc) > 0:
            tmp_max = max(CAC_loc)
            if len(CAC_800_loc) == 0:
               CAC_800_loc.append(tmp_max)
            elif tmp_max + 1 in CAC_800_loc:
               CAC_800_loc.append(tmp_max)
            else:
               break
            
            CAC_loc.remove(tmp_max)
        
        CAC_800 = [min(CAC_800_loc), max(CAC_800_loc)]
        
        if round(statistics.median(CAC_800)) < (dcm_array.shape[2] / 2):
            flipped = 1
        else:
            flipped = 0
        
        CAC_800_length = (CAC_800[1] - CAC_800[0]) * header.SliceThickness
        if CAC_800_length > 10:
            CAC_correct = False
        else:
            CAC_correct = True
        
        return slice_dict, array, threshold_kernel_central, CAC_800, CAC_correct, flipped
    
    
    
    def calcium_image_HA(self):
        # threshold array according to calcium_threshold 
        # simultaneously make image binary
        fact = 1.8
        slice_dict = {}
        threshold_used = self.calcium_threshold
        
        if (C.calcium_threshold == 'monoE' and threshold_used < 130):
            threshold_used = 110
        else:
            pass
        
        slice_dict, array, threshold_kernel_central, CAC_800, CAC_correct, flipped =\
            self.find_CAC_in_HA_artery(self.array, fact, slice_dict, self.header, threshold_used)
            
        if flipped == 1:
            slice_dict = {}
            self.array = np.flip(self.array,2)
            slice_dict, array, threshold_kernel_central, CAC_800, CAC_correct, _ =\
            self.find_CAC_in_HA_artery(self.array, fact, slice_dict, self.header, threshold_used)

        while CAC_correct == False:
            slice_dict = {}
            fact = fact * 1.1
            slice_dict, array, threshold_kernel_central, CAC_800, CAC_correct, _ =\
                self.find_CAC_in_HA_artery(self.array, fact, slice_dict, self.header, threshold_used)
        
        CAC_loc = {}
        CAC_loc['CAC_800'] = round(statistics.mean(CAC_800))
        CAC_loc['CAC_400'] = round(CAC_loc['CAC_800'] - 10 / self.header.SliceThickness)
        CAC_loc['CAC_200'] = round(CAC_loc['CAC_800'] - 20 / self.header.SliceThickness)
        CAC_loc['CAC_100'] = round(CAC_loc['CAC_800'] - 30 / self.header.SliceThickness)
        CAC_loc['CAC_75'] = round(CAC_loc['CAC_800'] - 40 / self.header.SliceThickness)
        
        self.CAC_loc = CAC_loc
        
        # print(CAC_loc)
        
            
        # create calcium image
        array = self.array.copy()
        array[array < self.calcium_threshold] = 0
        array[array > 0] = 1
        array = array.astype(dtype = np.uint8)
        self.calcium_image = array * self.array
        self.threshold_kernel_central = threshold_kernel_central
        
        return None
    
    
    
    def calc_center_CAC(self, plot_centers = False):
        center_CAC = {}
        for key in self.CAC_loc:
            used_slice = round(self.CAC_loc[key])
        
            array = self.array[:,:,used_slice].copy()
            array[array < self.threshold_kernel_central[0]] = 0
            array[array > 0] = 1
            array = array.astype(dtype = np.uint8)
            array_filtered = scipy.signal.medfilt2d(array, self.threshold_kernel_central[1])
            
            
            output = cv2.connectedComponentsWithStats(array_filtered, 4, cv2.CV_32S)
            
            center = [round(output[3][1][0]), round(output[3][1][1])]
            
            if plot_centers:
                plt.figure()
                plt.subplot()
                plt.imshow(self.array[:,:,used_slice])
                plt.plot(center[0], center[1], "x")
                plt.show()
        
            center_CAC[key] = center
        
        center_CAC['water'] = [round((center_CAC['CAC_800'][0] +  center_CAC['CAC_400'][0]) / 2), \
                               round((center_CAC['CAC_800'][1] +  center_CAC['CAC_400'][1]) / 2)]
        self.center_CAC = center_CAC
        
        return None
    
    
    
    def FWHM(self, length = 8):
        CAC_loc = {'water': round(self.CAC_loc['CAC_800'] - 5 / self.header.SliceThickness)}
        CAC_loc.update(self.CAC_loc)
        
        lumen_diameter = {}
        rel_diameter = {}
        
        for key in CAC_loc:
            used_slice = round(CAC_loc[key])
           
            array = self.array[:,:,used_slice].copy()
            
            FWHM = []
            
            
            for angle in range(0, 12, 1):
            
                [start1, stop1] = [int(self.center_CAC[key][0] + (length * math.cos(math.pi / 6 * angle)) / self.pixel_size), \
                                   int(self.center_CAC[key][1]+ (length * math.sin(math.pi / 6 * angle)) / self.pixel_size)]
                [start2, stop2] = [int(self.center_CAC[key][0] - (length * math.cos(math.pi / 6 * angle)) / self.pixel_size), \
                                   int(self.center_CAC[key][1]- (length * math.sin(math.pi / 6 * angle)) / self.pixel_size)]
                            
                profile = profile_line(array,[start1, stop1], [start2, stop2], mode = 'constant')
                
                peaks,_ = find_peaks(profile)
                result = scipy.signal.peak_widths(profile, peaks, rel_height = 0.5)
                if max(result[0]) * self.pixel_size > 10:
                    continue
                if key == ['CAC_400']: #ik krijg nog geen plots, maar onderstaande regels zouden de profile moeten geven inclusief peaks en FWHM
                    plt.subplot()
                    plt.plot(profile)
                    plt.plot(peaks, profile[peaks], "x")
                    plt.hlines(*result[1:], color = "C2")
                    plt.show()
                    
                FWHM.append(max(result[0]) * self.pixel_size)
                
            mean_FWHM = np.mean(FWHM)
            SD_FWHM = np.std(FWHM)
            lumen_diameter[key] = [mean_FWHM, SD_FWHM]
            rel_diameter[key] = lumen_diameter[key][0] / lumen_diameter['water'][0]
            
        
        self.lumen_diameter = lumen_diameter
        self.rel_diameter = rel_diameter

        return None
    
    
    
    def mask_scoring_def(self):
        used_slice = round(self.CAC_loc['CAC_800'])
        
        array = self.array[:,:,used_slice].copy()
        array[array < self.threshold_kernel_central[0]] = 0
        array[array > 0] = 1
        array = array.astype(dtype = np.uint8)
        array_filtered = scipy.signal.medfilt2d(array, self.threshold_kernel_central[1])
        
        # plt.imshow(array_filtered)
        # plt.show()
        
        output = cv2.connectedComponentsWithStats(array_filtered, 4, cv2.CV_32S)
        
        center_CAC_800 = [round(output[3][1][0]), round(output[3][1][1])] 
        
        mask_area = pat.create_circular_mask(array.shape[0], array.shape[1], center_CAC_800, 15 / self.pixel_size)
        mask_motion = pat.create_circular_mask(array.shape[0], array.shape[1], center_CAC_800, 20 / self.pixel_size)
        
        # plt.imshow(array - mask_area)
        # plt.show()
        
        self.mask_scoring = mask_area
        self.center_CAC_800 = center_CAC_800
        self.mask_motion = mask_motion
        
        return None
    
    
    def hollow_artery_CAC_scoring(self, calc_min, calc_max):
        if calc_min < 0:
            calc_min = 0
        if calc_max > self.array.shape[2]:
            calc_max = self.array.shape[2]
            
        # print(calc_min, calc_max)
            
        calc_array = self.array[:,:,calc_min:calc_max]
        calcium_calc_array = self.calcium_image[:,:,calc_min:calc_max]
        
        calc_array_binary = calc_array.copy()
        calc_array_binary[calc_array_binary < self.calcium_threshold] = 0
        calc_array_binary[calc_array_binary > 0] = 1
        calc_array_binary = calc_array_binary.astype(dtype = np.uint8)
    
        if (C.Volume_score_calc and ('SIEMENS' in self.header.Manufacturer.upper() or self.header.Manufacturer == 'Literature')): 
            calc_int_min = int(calc_min * self.int_factor + self.int_factor / 2)
            calc_int_max = int(calc_max * self.int_factor - self.int_factor / 2)
            calc_array_int = self.int_array[:,:,calc_int_min:calc_int_max]
            
            calc_array_int_binary = calc_array_int.copy()
            calc_array_int_binary[calc_array_int_binary < self.calcium_threshold] = 0
            calc_array_int_binary[calc_array_int_binary > 0] = 1
            calc_array_int_binary = calc_array_int_binary.astype(dtype = np.uint8)
            
            # Volume score calculation
            Volume_score = 0
            for slice_index in range(calc_array_int.shape[2]):
                output = cv2.connectedComponentsWithStats(calc_array_int_binary[:,:,slice_index],\
                                                              C.comp_connect, cv2.CV_32S)
        
                for slice_index2 in range(1,output[0]):
                    coordinates = int(output[3][slice_index2][1]), int(output[3][slice_index2][0])
                    area = output[2][slice_index2][4] * self.int_header.PixelSpacing[0]**2
                    
                    min_area = cac.min_area_det(self.header.Manufacturer, self.pixel_size)
                    
                    if area > min_area:
                        VS = area * self.int_header.SliceThickness
                        if self.mask_scoring[coordinates] != False:
                            Volume_score += VS
                        else:
                            pass
                    else:
                        pass
        else:
            Volume_score = 0
              
        # Agatston and Mass score calculation
        Agatston_score = 0
        Mass_score = 0
        num_voxels = 0
        mean_calc_tmp = 0
        max_calc_tmp = []
        for slice_index in range(calc_array.shape[2]):
            output = cv2.connectedComponentsWithStats(calc_array_binary[:,:,slice_index],\
                                                          C.comp_connect, cv2.CV_32S)
                
            for slice_index2 in range(1,output[0]):
                coordinates = int(output[3][slice_index2][1]), int(output[3][slice_index2][0])
                area = output[2][slice_index2][4] * self.header.PixelSpacing[0]**2
                num_vox_calc = output[2][slice_index2][4]
                   
    
                min_area = cac.min_area_det(self.header.Manufacturer, self.pixel_size)
                        
                if area > min_area:
                    if self.mask_scoring[coordinates] != False:
                        AS_weight = cac.AS_weight_calc(self.mask_scoring, calcium_calc_array[:,:,slice_index], C.calcium_threshold, self.header.ImageType)
                        Agatston_score += AS_weight * area
                        
                        MS = area * self.header.SliceThickness * cac.mass_mean_def(self.mask_scoring * calcium_calc_array[:,:,slice_index], self.calcium_threshold)
                        Mass_score += MS
                        
                        num_voxels += num_vox_calc
    
                        tmp_mean_array = output[1].copy()
                        tmp_mean_array[tmp_mean_array != slice_index2] = 0
                        tmp_mean_array[tmp_mean_array != 0] = 1
                        tmp_mean_array = tmp_mean_array * self.mask_scoring
                        tmp_mean_array = tmp_mean_array * calcium_calc_array[:,:,slice_index]
                        tmp_mean_array[tmp_mean_array < self.calcium_threshold] = 0
                        mean_calc_tmp += tmp_mean_array.sum()
                        max_calc_tmp.append(tmp_mean_array.max())
                    else:
                        pass
                else:
                    pass  
    
        if (self.header.Manufacturer.upper() == 'PHILIPS' or self.header.Manufacturer.upper() == 'CANON'):
            Mass_score = 0
            Volume_score = 0
            num_voxels = 0
            mean_calc_tmp = 0
            
            min_area = cac.min_area_det(self.header.Manufacturer, self.pixel_size)
            
            calc_array_binary = calc_array.copy()
            if self.header.Manufacturer == 'PHILIPS':
                calc_array_binary[calc_array_binary < C.Philips_mass_score_threshold] = 0
            else:
                calc_array_binary[calc_array_binary < self.calcium_threshold] = 0
            calc_array_binary[calc_array_binary > 0] = 1
            calc_array_binary = calc_array_binary.astype(dtype = np.uint8)
            
            # Volume and Mass score calculation
            for slice_index in range(calc_array.shape[2]):
                output = cv2.connectedComponentsWithStats(calc_array_binary[:,:,slice_index],\
                                                              C.comp_connect, cv2.CV_32S)
                
                for slice_index2 in range(1,output[0]):
                    coordinates = int(output[3][slice_index2][1]), int(output[3][slice_index2][0])
                    area = output[2][slice_index2][4] * self.pixel_size**2 
                    num_vox_calc = output[2][slice_index2][4]
                    
                    mask_mean_mass = output[1].copy()
                    mask_mean_mass[mask_mean_mass != slice_index2] = 0
                    mask_mean_mass[mask_mean_mass != 0] = 1
    
                    if area > min_area:
                        MS = area * self.header.SliceThickness * cac.mass_mean_def(mask_mean_mass * calcium_calc_array[:,:,slice_index], self.calcium_threshold)
                        VS = area * self.header.SliceThickness
                        
                        
                        if self.mask_scoring[coordinates] != False:
                            MS = area * self.header.SliceThickness * cac.mass_mean_def(self.mask_scoring * calcium_calc_array[:,:,slice_index], self.calcium_threshold)
                            VS = area * self.header.SliceThickness
                            
                            Mass_score += MS 
                            Volume_score += VS
                            
                            num_voxels += num_vox_calc
                            
                            tmp_mean_array = output[1].copy()
                            tmp_mean_array[tmp_mean_array != slice_index2] = 0
                            tmp_mean_array[tmp_mean_array != 0] = 1
                            tmp_mean_array = tmp_mean_array * self.mask_scoring
                            tmp_mean_array = tmp_mean_array * calcium_calc_array[:,:,slice_index]
                            tmp_mean_array[tmp_mean_array < self.calcium_threshold] = 0
                            mean_calc_tmp += tmp_mean_array.sum()
                    else:
                        pass      
                
        Volume_score = round(Volume_score, 5)
        Mass_score = round(Mass_score * self.mass_cal_factor, 5)
        Agatston_score = round(Agatston_score * self.header.SliceThickness / 3, 5)
        
        try:
            max_calc_tmp = max(max_calc_tmp)
        except:
            max_calc_tmp = 0
        
        try:
            mean_calc_tmp = round(mean_calc_tmp / num_voxels,1)
        except:
            mean_calc_tmp = 0
            
            
        return Agatston_score, Mass_score, Volume_score, mean_calc_tmp, num_voxels, max_calc_tmp
    
    
    def hollow_artery_scoring(self):
        CAC_scores = {}
        
        for key in self.CAC_loc.keys():
            if key == 'CAC_800':
                min_slice = int(self.CAC_loc[key]) - round(4 / self.header.SliceThickness)
                max_slice = math.ceil(self.CAC_loc[key]) + round(4 / self.header.SliceThickness)
            else:
                min_slice = self.CAC_loc[key] - round(4 / self.header.SliceThickness)
                max_slice = self.CAC_loc[key] + round(4 / self.header.SliceThickness)
                
            CAC_scores[key] = self.hollow_artery_CAC_scoring(min_slice, max_slice)
        
        self.CAC_scores = CAC_scores            
        return None
    
    
    def arteries_quality(self):        
        used_slice = self.CAC_loc['CAC_75'] +\
            int((self.CAC_loc['CAC_100'] - self.CAC_loc['CAC_75'])/2)
        
        
        array = self.array[:,:,used_slice].copy()
        # plt.imshow(array)
        # plt.show()
        
        # plt.imshow(array * (1 - self.mask_scoring))
        # plt.show()
        
        start_ROI = self.center_CAC_800
        
        dcm_quality = array[start_ROI[0]- int(30 / self.pixel_size):start_ROI[0]- int(15 / self.pixel_size),\
          int(self.header.Columns/2 - 15/self.pixel_size):int(self.header.Columns/2 + 15/self.pixel_size)]
        
        qc = {}
        qc['mean'] = round(dcm_quality.mean(),5)
        qc['SD'] = round(dcm_quality.std(),5)
     
        array_binary = dcm_quality.copy()
        array_binary[array_binary < self.calcium_threshold] = 0
        array_binary[array_binary > 0] = 1
        array_binary = array_binary.astype(dtype = np.uint8)
        
        output = cv2.connectedComponentsWithStats(array_binary, C.comp_connect, cv2.CV_32S)
        qc_areas = []
        for index in range(output[0]):
            qc_areas.append(output[2][index][4])
            
        qc_max_area = qc_areas.index(max(qc_areas))
        
        AS_slice = 0        
        for component_index in range(1,output[0]):
            if component_index != qc_max_area:
                tmp_array = output[1].copy()
                tmp_array[tmp_array != component_index] = 0
                tmp_array = tmp_array * dcm_quality
    
                area = output[2][component_index][4] * self.pixel_size**2
                
                min_area = cac.min_area_det(self.header.Manufacturer, self.pixel_size)
        
                if area > min_area:
                    AS_weight = cac.AS_weight_calc(np.ones_like(tmp_array), tmp_array, C.calcium_threshold, self.header.ImageType)
                    AS = area * AS_weight
                    AS_slice += AS                 
            
        qc['AS'] = round(AS_slice,5)
        
        # # Calculate NPS
        # qc['NPS'] = pat.nps_calc(self.array_ori[:,:,int((self.calc1[1] + self.calc2[0]) / 2)].copy(), self.pixel_size, self.center_insert,\
        #                           num_ROIs = 18, rad = 32, ROI_size = 15, plot_NPS = False)
        
        self.qc = qc
    
        return None
    
    @staticmethod
    def calc_motion_loc(loc_200, loc_100):
                
        if loc_200 > loc_100:
            loc_motion = math.ceil(loc_200 - (loc_200 - loc_100) / 2)
        else:
            loc_motion = math.ceil(loc_100 - (loc_100 - loc_200) / 2)
            
        return loc_motion
    
    
    def motion_character(self):
        loc_motion = self.calc_motion_loc(self.CAC_loc['CAC_200'], self.CAC_loc['CAC_100'])
        threshold = self.qc['mean'] + 5 * self.qc['SD'] 
        print(threshold)
        # threshold = self.calcium_threshold
        
        motion_slice = self.array[:,:,loc_motion].copy()
               
        center = self.center_CAC_800
        spacing = math.ceil(20 / self.pixel_size)
        motion_img = motion_slice[center[1]- spacing: center[1] + spacing,\
                                  center[0] - spacing:center[0] + spacing]
        
        motion_slice = motion_slice * self.mask_motion
        
        motion_thresholded = motion_slice.copy()
        motion_thresholded[motion_thresholded < threshold] = 0
        
        motion_binary = motion_slice.copy()
        motion_binary[motion_binary < threshold] = 0
        motion_binary[motion_binary > 0] = 1
        motion_binary = motion_binary.astype(dtype = np.uint8)
        
        motion_img_binary = motion_binary[center[1]- spacing: center[1] + spacing,\
                                          center[0] - spacing:center[0] + spacing]
        
        output = cv2.connectedComponentsWithStats(motion_binary, 4, cv2.CV_32S)
        
        area_index = []
        for index in range(output[0]):
            area_index.append(output[2][index][4])
        
        sorted_area_index = area_index.copy()
        sorted_area_index.sort()
        
        index_largest = area_index.index(sorted_area_index[output[0] - 2])
        
        # print(area_index.sort())
        
        area = output[2][index_largest][4]
        total_area = np.count_nonzero(motion_binary)
        bounding_box = [output[2][index_largest][2], output[2][index_largest][3]]
        
        motion_contour = output[1]
        motion_contour[motion_contour != index_largest] = 0
        motion_contour = motion_contour.astype(dtype = np.uint8)        
        
        contour, _ = cv2.findContours(motion_contour, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        
        for c in contour:
            arc_length = cv2.arcLength(c, True)
        arc_length = round(arc_length, 2)
        
        # self.motion_area = np.count_nonzero(motion_binary)
        print('\n',area, total_area, bounding_box, arc_length, '\n')
        
        plt.imshow(motion_img)
        plt.show()
        
        plt.imshow(motion_img_binary)
        plt.show()
        
        return None
