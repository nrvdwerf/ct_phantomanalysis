# main document CCI insert analysis

import pydicom
# import SimpleITK as sitk
import numpy as np
# import scipy
import matplotlib.pyplot as plt
import cv2
import math
import statistics
from ctp.Phantom_analysis_toolbox import Phantom_analysis_toolbox as pat
from ctp.Phantom_analysis_toolbox import CAC_scoring as cac
from ctp.qrm_phantoms.Thorax import QRM_thorax_analyser as thorax
import constants as C
from skimage.transform import rotate
from scipy import optimize
import sympy

  

    
class CCI_analyser(thorax):
    def __init__(self, used_path):
        super().__init__(used_path)
        
        self.CCI_calcium_image()
        self.CCI_scoring()
        self.calcium_quality_CCI()
        self.Measurement_calrod()
        if C.MTF_calc == True:
            self.MTF_calc(plot_MTF = False)
            
        return None
    
    def CCI_calcium_image(self):
        array = pat.dcm_threshold(self.array.copy(),\
                              self.calcium_threshold,\
                                  multiplier = 1.1)
        array = array.astype(dtype = np.uint8)
        
        CCI_5mm_num_pixels = int(math.pi * (5/2)**2 / self.pixel_size**2)
        cal_rod_num_pixels = int(math.pi * (20/2)**2 / self.pixel_size**2)
    
        im_kernel = math.ceil(5 / self.pixel_size)

        slice_dict =  {}
        large_index = []
        cal_rod_dict = {}
        for idx in range(array.shape[2]):
            array_filtered = pat.dcm_filtering(array[:,:,idx], image_kernel = im_kernel)
            output = pat.arr_connected_components(array_filtered)
            
            count_5mm = 0
            for index in range(1,output[0]):
                if output[2][index][4] in range(int(CCI_5mm_num_pixels * 0.6),\
                         int(CCI_5mm_num_pixels * 1.5)):
                    count_5mm += 1
                elif output[2][index][4] in range(int(cal_rod_num_pixels * 0.7),\
                         int(cal_rod_num_pixels * 1.3)):
                    cal_rod_dict[index] = [int(output[3][index][1]), int(output[3][index][0])]
            if (count_5mm > 0 and count_5mm < 4):
                slice_dict[idx] = count_5mm
                    
            cal_rod_dict = pat.pop_noncircular_objects(array_filtered,\
                                                   cal_rod_dict,\
                                                       sphere_est = 0.3)
                    
            if len(cal_rod_dict) == 0:
                pass
            else:
                large_index.append(idx)
                
        flipped_index = int(statistics.median(large_index))
    
        edge_index = []
        if flipped_index < (self.array.shape[2] / 2):
            flipped = -1
            for element in large_index:
                if element > (self.array.shape[2] / 2):
                    edge_index.append(element)
            if not edge_index:
                pass
            else:
                for index_edge in range(min(edge_index), self.array.shape[2]):
                    try:
                        del(slice_dict[index_edge])
                    except:
                        pass
                for element2 in edge_index:
                    large_index.remove(element2)
                    
            for element in range(max(large_index)):
                try:
                    del(slice_dict[element])
                except:
                    pass        
        else:
            flipped = 1
            for element in large_index:
                if element < (self.array.shape[2] / 2):
                    edge_index.append(element)
            if not edge_index:
                pass
            else:
                for index_edge in range(max(edge_index)):
                    try:
                        del(slice_dict[index_edge])
                    except:
                        pass
                for element2 in edge_index:
                    large_index.remove(element2)
            for element in range(min(large_index), self.array.shape[2]):
                try:
                    del(slice_dict[element])
                except:
                    pass
        
        poppable_keys = []        
        if flipped == -1:
            for key in slice_dict.keys():
                if key > (flipped_index + (55 / self.header.SliceThickness)):
                    poppable_keys.append(key)
        elif flipped == 1:
            for key in slice_dict.keys():
                if key < (flipped_index - (55 / self.header.SliceThickness)):
                    poppable_keys.append(key)
        for key in poppable_keys:
                    slice_dict.pop(key)            
                    
           
        max_key, _ = max(zip(slice_dict.values(), slice_dict.keys()))
    
        max_keys = []
        for key in slice_dict.keys():
            if slice_dict[key] is max_key:
                max_keys.append(key)
        
        self.slice_CCI = int(statistics.median(max_keys))
        
        self.calcium_image = self.array_ori * pat.dcm_threshold(self.array.copy(),\
                              self.calcium_threshold) 
        
        self.quality_slice = round(self.slice_CCI - flipped * (20 / self.header.SliceThickness))
    
        self.cal_rod_slice = self.slice_CCI + (flipped * int(30 / self.header.SliceThickness))
        self.flipped = flipped
        
        return None
    
    @staticmethod
    def angle_calc(side1, side2):
        #Calculate angle between two sides of rectangular triangle
        if side1 == 0:
            angle = 0
        elif side2 == 0:
            angle = math.pi / 2
        else:
            angle = math.atan(side1 / side2)
        
        return angle    
    
    
    def calc_centers(self, output, tmp_center, CCI_array):
        sizes = []
        for size_index in range(1,len(output[2])):
            sizes.append(output[2][size_index][4])
    
        largest = {}
        for index in range(1,len(output[3])):
            dist_loc = math.sqrt((tmp_center[1] - output[3][index][0])**2 +\
                                 (tmp_center[0] - output[3][index][1])**2)
            dist_loc *= self.header.PixelSpacing[0]
            if dist_loc > 31:
                largest[index] = [int(output[3][index][1]),int(output[3][index][0])]
            else:
                pass
    
        max_dict = {}
        for key in largest.keys():
            tmp_arr = pat.create_circular_mask(self.header.Rows, self.header.Columns,\
                                           [largest[key][1],largest[key][0]],\
                                           math.ceil(2.5 / self.header.PixelSpacing[0]))
            tmp_arr = tmp_arr * self.array[:,:,self.slice_CCI] +\
                        tmp_arr * self.array[:,:,self.slice_CCI - 1] +\
                        tmp_arr * self.array[:,:,self.slice_CCI + 1]
            tmp_arr[tmp_arr == 0] = np.nan
            max_dict[key] = np.nanmedian(tmp_arr)
    
        large1_index, large1_key = max(zip(max_dict.values(), max_dict.keys()))
        max_dict.pop(large1_key)
        large2_index, large2_key = max(zip(max_dict.values(), max_dict.keys()))
        max_dict.pop(large2_key)
        large3_index, large3_key = max(zip(max_dict.values(), max_dict.keys()))
    
        center1 = largest[large1_key]
        center2 = largest[large2_key]  
        center3 = largest[large3_key]       

        self.center_insert = self.findCircle(center1, center2, center3)
        
        center = self.center_insert
        centers = {}
        for size_index4 in (center1, center2, center3):
            center_index = size_index4
            side_x = abs(center[0]-center_index[0])
            side_y = abs(center[1]-center_index[1])
            
            angle = self.angle_calc(side_x, side_y)
            if (center_index[0] < center[0] and center_index[1] < center[1]):
                    medium_calc = [int(center_index[0] + (12.5 / self.pixel_size) * math.sin(angle)),\
                                   int((center_index[1] + (12.5 / self.pixel_size) * math.cos(angle)))]
                
                    low_calc = [int(center_index[0] + (25 / self.pixel_size) * math.sin(angle)),\
                                int((center_index[1] + (25 / self.pixel_size) * math.cos(angle)))]
            elif (center_index[0] < center[0] and center_index[1] > center[1]):
                    medium_calc = [int(center_index[0] + (12.5 / self.pixel_size) * math.sin(angle)),\
                                   int((center_index[1] - (12.5 / self.pixel_size) * math.cos(angle)))]
                
                    low_calc = [int(center_index[0] + (25 / self.pixel_size) * math.sin(angle)),\
                                int((center_index[1] - (25 / self.pixel_size) * math.cos(angle)))] 
            elif (center_index[0] > center[0] and center_index[1] < center[1]):
                    medium_calc = [int(center_index[0] - (12.5 / self.pixel_size) * math.sin(angle)),\
                                   int((center_index[1] + (12.5 / self.pixel_size) * math.cos(angle)))]
                
                    low_calc = [int(center_index[0] - (25 / self.pixel_size) * math.sin(angle)),\
                                int((center_index[1] + (25 / self.pixel_size) * math.cos(angle)))]
            elif (center_index[0] > center[0] and center_index[1] > center[1]):
                    medium_calc = [int(center_index[0] - (12.5 / self.pixel_size) * math.sin(angle)),\
                                   int((center_index[1] - (12.5 / self.pixel_size) * math.cos(angle)))]
                
                    low_calc = [int(center_index[0] - (25 / self.pixel_size) * math.sin(angle)),\
                                int((center_index[1] - (25 / self.pixel_size) * math.cos(angle)))]
            elif (side_x == 0 and center_index[1] < center[1]):
                    medium_calc = [int(center_index[0]), int(center_index[1] + (12.5 / self.pixel_size))]
                
                    low_calc = [int(center_index[0]), int(center_index[1] + (25 / self.pixel_size))]
            elif (side_x == 0 and center_index[1] > center[1]):
                    medium_calc = [int(center_index[0]), int(center_index[1] - (12.5 / self.pixel_size))]
                
                    low_calc = [int(center_index[0]), int(center_index[1] - (25 / self.pixel_size))]
            elif (center_index[0] > center[0] and side_y == 0):
                    medium_calc = [int(center_index[0] - (12.5 / self.pixel_size)), int(center_index[1])]
                
                    low_calc = [int(center_index[0] - (25 / self.pixel_size)), int(center_index[1])]
            elif (center_index[0] > center[0] and side_y == 0):
                    medium_calc = [int(center_index[0] + (12.5 / self.pixel_size)), int(center_index[1])]
                
                    low_calc = [int(center_index[0] + (25 / self.pixel_size)), int(center_index[1])]
            else:
                    print("unknown angle.. error!")
                    
            if size_index4 == center1:
                centers['Large_HD'] = ([center_index])
                centers['Medium_HD'] = ([medium_calc])
                centers['Small_HD'] = ([low_calc])            
            
            elif size_index4 == center2:
                centers['Large_MD'] = ([center_index])
                centers['Medium_MD'] = ([medium_calc])
                centers['Small_MD'] = ([low_calc])    
            
            elif size_index4 == center3:
                centers['Large_LD'] = ([center_index])
                centers['Medium_LD'] = ([medium_calc])
                centers['Small_LD'] = ([low_calc])   
            
            else:
                pass
            
        self.calc_size_density_VS_AS_MS = centers
        
        return None
    
    
    def mass_calibration(self, CCI_array):
        # Calculate mass calibration factor
        center_LD = [self.calc_size_density_VS_AS_MS['Large_LD'][0][0],\
                     self.calc_size_density_VS_AS_MS['Large_LD'][0][1]]
    
        center = self.center_insert
        
        dist_x = abs(center_LD[0] - center[0])
        dist_y = abs(center_LD[1] - center[1])
    
        if dist_x == 0:
             mass_center_x = center[0]
             if center_LD[1] > center[1]:
                 mass_center_y = math.ceil(center[1] - math.ceil(23 / self.header.PixelSpacing[0]))
             else:
                 mass_center_y = math.ceil(center[1] + math.ceil(23 / self.header.PixelSpacing[0]))
        elif dist_y == 0:
            mass_center_y = center[1]
            if center_LD[0] > center[0]:
                 mass_center_x = math.ceil(center[0] - math.ceil(23 / self.header.PixelSpacing[0]))
            else:
                 mass_center_x = math.ceil(center[0] + math.ceil(23 / self.header.PixelSpacing[0]))
        
        else:
            mass_angle = math.atan(dist_y / dist_x)
            dist_x = (23 / self.header.PixelSpacing[0]) * math.cos(mass_angle)
            dist_y = (23 / self.header.PixelSpacing[0]) * math.sin(mass_angle)
        
            if (center_LD[0] < center[0] and center_LD[1] < center[1]):
                mass_center_x = math.ceil(center[0] + dist_x)
                mass_center_y = math.ceil(center[1] + dist_y)
            elif (center_LD[0] < center[0] and center_LD[1] > center[1]):
                mass_center_x = math.ceil(center[0] + dist_x)
                mass_center_y = math.ceil(center[1] - dist_y)
            elif (center_LD[0] > center[0] and center_LD[1] < center[1]):
                mass_center_x = math.ceil(center[0] - dist_x)
                mass_center_y = math.ceil(center[1] + dist_y)
            elif (center_LD[0] > center[0] and center_LD[1] > center[1]):
                mass_center_x = math.ceil(center[0] - dist_x)
                mass_center_y = math.ceil(center[1] - dist_y)
            
        mass_cal_center = [mass_center_y, mass_center_x]
        
        x_distance = abs(center[0] - mass_cal_center[1])
        angled_distance = math.sqrt((center[0] - mass_cal_center[1])**2 +\
                                    (center[1] - mass_cal_center[0])**2)
        angle_0_200HA = math.acos(x_distance / angled_distance) * 180 / math.pi
    
        mask_0HU = pat.create_circular_mask(self.header.Columns, self.header.Rows,\
                                        mass_cal_center, int(6.9/ self.header.PixelSpacing[0]))
        
        masked_0HU = mask_0HU * self.array[:,:, self.cal_rod_slice]
        mean_0HU = masked_0HU.sum() / np.count_nonzero(masked_0HU)
        
        std_0HU = 0
        for voxel in masked_0HU.flatten():
            if voxel != 0:
                std_0HU += (voxel - mean_0HU)**2
        std_0HU = math.sqrt(std_0HU / np.count_nonzero(masked_0HU))
        
        mask_200HU = pat.create_circular_mask(self.header.Columns, self.header.Rows,\
                                          [center[1], center[0]], int(6.9/ self.header.PixelSpacing[0]))
        
        masked_200HU = mask_200HU * self.array[:,:,self.cal_rod_slice]
        mean_200HU = masked_200HU.sum() / np.count_nonzero(masked_200HU)
        
        mass_cal_factor = 0.2 / (mean_200HU - mean_0HU)
        
        water_rod_metrics = [mean_0HU, std_0HU]
        
        self.mass_cal_factor = mass_cal_factor
        self.angle_0_200HA = angle_0_200HA
        self.water_rod_metrics = water_rod_metrics
      
        return None
    
    def CCI_scoring(self, print_plot = False):
        # Actual scoring for CCI insert
        # First step is to remove slices without calcium from arrays            
        CCI_min = int((self.slice_CCI - math.ceil(5 / self.header.SliceThickness)) - 1)
        CCI_max = int((self.slice_CCI + math.ceil(5 / self.header.SliceThickness)) + 1)
        central_CCI = int((CCI_max - CCI_min)/2)
        
        if CCI_min < 0:
            CCI_min = 0
        if CCI_max > self.array.shape[2]:
            CCI_max = self.array.shape[2]
        
        CCI_array = self.array[:,:,CCI_min:CCI_max].copy()
        
        CCI_array_binary = pat.dcm_threshold(CCI_array.copy(), self.calcium_threshold)
        
        conn_comp = pat.arr_connected_components(CCI_array_binary[:,:,central_CCI - 1] +\
                                                 CCI_array_binary[:,:,central_CCI] +\
                                                 CCI_array_binary[:,:,central_CCI + 1])
        centroids = conn_comp[3]
        centroids = np.delete(centroids,0,0)

        CCI_Agatston_scores = {}
        CCI_Volume_scores = {}
        for index in range(len(centroids)):
            CCI_Agatston_scores[index] = 0
            CCI_Volume_scores[index] = 0
    
        
    
        kern = math.ceil(3 / self.header.PixelSpacing[0])
            
        image_for_center = pat.dcm_filtering(CCI_array_binary[:,:,central_CCI - 1], image_kernel = kern) +\
                            pat.dcm_filtering(CCI_array_binary[:,:,central_CCI], image_kernel = kern) +\
                            pat.dcm_filtering(CCI_array_binary[:,:,central_CCI + 1], image_kernel = kern)
                           

        output = pat.arr_connected_components(image_for_center)
    
        tmp_center = self.center_insert.copy()
        self.calc_centers(output, tmp_center, CCI_array)
        self.mass_calibration(CCI_array)
            
        for key in self.calc_size_density_VS_AS_MS.keys():
            self.calc_size_density_VS_AS_MS[key].append(0)
            self.calc_size_density_VS_AS_MS[key].append(0)
            self.calc_size_density_VS_AS_MS[key].append(0)
        
        mask_L_HD = pat.create_circular_mask(self.header.Columns, self.header.Rows, [self.calc_size_density_VS_AS_MS['Large_HD'][0][1],\
                            self.calc_size_density_VS_AS_MS['Large_HD'][0][0]],math.ceil((5 / self.header.PixelSpacing[0])/2) + 1)
        mask_L_MD = pat.create_circular_mask(self.header.Columns, self.header.Rows, [self.calc_size_density_VS_AS_MS['Large_MD'][0][1],\
                            self.calc_size_density_VS_AS_MS['Large_MD'][0][0]],math.ceil((5 / self.header.PixelSpacing[0])/2) + 1)
        mask_L_LD = pat.create_circular_mask(self.header.Columns, self.header.Rows, [self.calc_size_density_VS_AS_MS['Large_LD'][0][1],\
                            self.calc_size_density_VS_AS_MS['Large_LD'][0][0]],math.ceil((5 / self.header.PixelSpacing[0])/2) + 1)   
        mask_M_HD = pat.create_circular_mask(self.header.Columns, self.header.Rows, [self.calc_size_density_VS_AS_MS['Medium_HD'][0][1],\
                            self.calc_size_density_VS_AS_MS['Medium_HD'][0][0]],math.ceil((3 / self.header.PixelSpacing[0])/2) + 1)
        mask_M_MD = pat.create_circular_mask(self.header.Columns, self.header.Rows, [self.calc_size_density_VS_AS_MS['Medium_MD'][0][1],\
                            self.calc_size_density_VS_AS_MS['Medium_MD'][0][0]],math.ceil((3 / self.header.PixelSpacing[0])/2) + 1)
        mask_M_LD = pat.create_circular_mask(self.header.Columns, self.header.Rows, [self.calc_size_density_VS_AS_MS['Medium_LD'][0][1],\
                            self.calc_size_density_VS_AS_MS['Medium_LD'][0][0]],math.ceil((3 / self.header.PixelSpacing[0])/2) + 1) 
        mask_S_HD = pat.create_circular_mask(self.header.Columns, self.header.Rows, [self.calc_size_density_VS_AS_MS['Small_HD'][0][1],\
                            self.calc_size_density_VS_AS_MS['Small_HD'][0][0]],math.ceil((1 / self.header.PixelSpacing[0])/2) + 1)
        mask_S_MD = pat.create_circular_mask(self.header.Columns, self.header.Rows, [self.calc_size_density_VS_AS_MS['Small_MD'][0][1],\
                            self.calc_size_density_VS_AS_MS['Small_MD'][0][0]],math.ceil((1 / self.header.PixelSpacing[0])/2) + 1)
        mask_S_LD = pat.create_circular_mask(self.header.Columns, self.header.Rows, [self.calc_size_density_VS_AS_MS['Small_LD'][0][1],\
                            self.calc_size_density_VS_AS_MS['Small_LD'][0][0]],math.ceil((1 / self.header.PixelSpacing[0])/2) + 1) 
        
        
        masks1 = mask_L_HD + mask_M_HD + mask_S_HD
        masks2 = mask_L_MD + mask_M_MD + mask_S_MD
        masks3 = mask_L_LD + mask_M_LD + mask_S_LD
    
        if print_plot:
            plt.subplot(121)
            plt.imshow(self.calcium_image[:,:,self.CCI_slice])
            plt.subplot(122)
            plt.imshow(masks1 + masks2 + masks3)
            plt.show()
        
        mask_L_HD_BAS = pat.create_circular_mask(self.header.Columns, self.header.Rows, [self.calc_size_density_VS_AS_MS['Large_HD'][0][1],\
                            self.calc_size_density_VS_AS_MS['Large_HD'][0][0]],math.ceil((5*2 / self.header.PixelSpacing[0])/2) + 1)
        mask_L_MD_BAS = pat.create_circular_mask(self.header.Columns, self.header.Rows, [self.calc_size_density_VS_AS_MS['Large_MD'][0][1],\
                            self.calc_size_density_VS_AS_MS['Large_MD'][0][0]],math.ceil((5*2 / self.header.PixelSpacing[0])/2) + 1)
        mask_L_LD_BAS = pat.create_circular_mask(self.header.Columns, self.header.Rows, [self.calc_size_density_VS_AS_MS['Large_LD'][0][1],\
                            self.calc_size_density_VS_AS_MS['Large_LD'][0][0]],math.ceil((5*2 / self.header.PixelSpacing[0])/2) + 1)   
        mask_M_HD_BAS = pat.create_circular_mask(self.header.Columns, self.header.Rows, [self.calc_size_density_VS_AS_MS['Medium_HD'][0][1],\
                            self.calc_size_density_VS_AS_MS['Medium_HD'][0][0]],math.ceil((3*2 / self.header.PixelSpacing[0])/2) + 1)
        mask_M_MD_BAS = pat.create_circular_mask(self.header.Columns, self.header.Rows, [self.calc_size_density_VS_AS_MS['Medium_MD'][0][1],\
                            self.calc_size_density_VS_AS_MS['Medium_MD'][0][0]],math.ceil((3*2 / self.header.PixelSpacing[0])/2) + 1)
        mask_M_LD_BAS = pat.create_circular_mask(self.header.Columns, self.header.Rows, [self.calc_size_density_VS_AS_MS['Medium_LD'][0][1],\
                            self.calc_size_density_VS_AS_MS['Medium_LD'][0][0]],math.ceil((3*2 / self.header.PixelSpacing[0])/2) + 1) 
        mask_S_HD_BAS = pat.create_circular_mask(self.header.Columns, self.header.Rows, [self.calc_size_density_VS_AS_MS['Small_HD'][0][1],\
                            self.calc_size_density_VS_AS_MS['Small_HD'][0][0]],math.ceil((1*2 / self.header.PixelSpacing[0])/2) + 1)
        mask_S_MD_BAS = pat.create_circular_mask(self.header.Columns, self.header.Rows, [self.calc_size_density_VS_AS_MS['Small_MD'][0][1],\
                            self.calc_size_density_VS_AS_MS['Small_MD'][0][0]],math.ceil((1*2 / self.header.PixelSpacing[0])/2) + 1)
        mask_S_LD_BAS = pat.create_circular_mask(self.header.Columns, self.header.Rows, [self.calc_size_density_VS_AS_MS['Small_LD'][0][1],\
                            self.calc_size_density_VS_AS_MS['Small_LD'][0][0]],math.ceil((1*2 / self.header.PixelSpacing[0])/2) + 1) 
        
        BAS_Mask = mask_L_HD_BAS + mask_M_HD_BAS + mask_S_HD_BAS+\
                      mask_L_MD_BAS + mask_M_MD_BAS + mask_S_MD_BAS+\
                          mask_L_LD_BAS + mask_M_LD_BAS + mask_S_LD_BAS
                          
        self.BAS_Mask = np.invert(BAS_Mask)        
            
        # Volume score calculation
        if (C.Volume_score_calc and (self.scoring_method == 'SIEMENS' or self.scoring_method == 'Literature')):
            int_header, CCI_array_int = pat.dcm_interpolation(self.array[:,:,CCI_min:CCI_max], self.header)
            
            CCI_array_int_binary = pat.dcm_threshold(CCI_array_int, self.calcium_threshold)
            
            for slice_index in range(CCI_array_int.shape[2]):
                output = pat.arr_connected_components(CCI_array_int_binary[:,:,slice_index])
                    
                for slice_index2 in range(1,output[0]):
                    coordinates = int(output[3][slice_index2][1]), int(output[3][slice_index2][0])
                    area = output[2][slice_index2][4] * int_header.PixelSpacing[0]**2
                    
                    min_area = cac.min_area_det(self.scoring_method, self.pixel_size)
                    
                    if area > min_area:
                        VS = area * int_header.SliceThickness
                        
                        if mask_L_HD[coordinates] != False:
                            self.calc_size_density_VS_AS_MS['Large_HD'][1] += VS
                        elif mask_L_MD[coordinates] != False:
                            self.calc_size_density_VS_AS_MS['Large_MD'][1] += VS                
                        elif mask_L_LD[coordinates] != False:
                            self.calc_size_density_VS_AS_MS['Large_LD'][1] += VS                                  
                        elif mask_M_HD[coordinates] != False:
                            self.calc_size_density_VS_AS_MS['Medium_HD'][1] += VS                     
                        elif mask_M_MD[coordinates] != False:
                            self.calc_size_density_VS_AS_MS['Medium_MD'][1] += VS                    
                        elif mask_M_LD[coordinates] != False:
                            self.calc_size_density_VS_AS_MS['Medium_LD'][1] += VS                
                        elif mask_S_HD[coordinates] != False:
                            self.calc_size_density_VS_AS_MS['Small_HD'][1] += VS                    
                        elif mask_S_MD[coordinates] != False:
                            self.calc_size_density_VS_AS_MS['Small_MD'][1] += VS                    
                        elif mask_S_LD[coordinates] != False:
                            self.calc_size_density_VS_AS_MS['Small_LD'][1] += VS
                        else:
                            pass
                    else:
                        pass
        
        CCI_array_binary = pat.dcm_threshold(CCI_array.copy(), self.calcium_threshold)        
        
        calc_mean_dict = {}
        SNR_dict = {}
        # Agatston and Mass score calculation
        for slice_index in range(CCI_array.shape[2]):                
            output = pat.arr_connected_components(CCI_array_binary[:,:,slice_index])
            for slice_index2 in range(1,output[0]):
                coordinates = int(output[3][slice_index2][1]), int(output[3][slice_index2][0])
                area = output[2][slice_index2][4] * self.header.PixelSpacing[0]**2
                
                mask_mean_mass = output[1].copy()
                mask_mean_mass[mask_mean_mass != slice_index2] = 0
                mask_mean_mass[mask_mean_mass != 0] = 1
    
                min_area = cac.min_area_det(self.scoring_method, self.pixel_size)
                if area > min_area:
                    MS = area * self.header.SliceThickness * cac.mass_mean_def(mask_mean_mass * CCI_array[:,:,slice_index], self.calcium_threshold)
                    
                    if mask_L_HD[coordinates] != False:
                        AS_weight = cac.AS_weight_calc(mask_L_HD, CCI_array[:,:,slice_index], C.calcium_threshold, self.header.ImageType)
                        self.calc_size_density_VS_AS_MS['Large_HD'][2] += AS_weight * area
                        
                        self.calc_size_density_VS_AS_MS['Large_HD'][3] += MS                        
                        calc_mean_dict = cac.calc_mean_calculator(calc_mean_dict, 'Large_HD', output[1].copy(),\
                                            CCI_array[:,:,slice_index].copy(), slice_index2)
                        
                        if slice_index == central_CCI:
                            SNR_dict = cac.SNR_dict_calculator(SNR_dict, 'Large_HD', output[1].copy(),\
                                            CCI_array[:,:,slice_index].copy(), slice_index2)    
                            
    
                    elif mask_L_MD[coordinates] != False:
                        AS_weight = cac.AS_weight_calc(mask_L_MD, CCI_array[:,:,slice_index], C.calcium_threshold, self.header.ImageType)
                        self.calc_size_density_VS_AS_MS['Large_MD'][2] += AS_weight * area
                        
                        self.calc_size_density_VS_AS_MS['Large_MD'][3] += MS                    
                        calc_mean_dict = cac.calc_mean_calculator(calc_mean_dict, 'Large_MD', output[1].copy(),\
                                            CCI_array[:,:,slice_index].copy(), slice_index2)
                            
                        if slice_index == central_CCI:
                            SNR_dict = cac.SNR_dict_calculator(SNR_dict, 'Large_MD', output[1].copy(),\
                                            CCI_array[:,:,slice_index].copy(), slice_index2)
                    
                    elif mask_L_LD[coordinates] != False:
                        AS_weight = cac.AS_weight_calc(mask_L_LD, CCI_array[:,:,slice_index], C.calcium_threshold, self.header.ImageType)
                        self.calc_size_density_VS_AS_MS['Large_LD'][2] += AS_weight * area
    
                        self.calc_size_density_VS_AS_MS['Large_LD'][3] += MS                     
                        calc_mean_dict = cac.calc_mean_calculator(calc_mean_dict, 'Large_LD', output[1].copy(),\
                                            CCI_array[:,:,slice_index].copy(), slice_index2)
                            
                                                
                        if slice_index == central_CCI:
                            SNR_dict = cac.SNR_dict_calculator(SNR_dict, 'Large_LD', output[1].copy(),\
                                            CCI_array[:,:,slice_index].copy(), slice_index2)
                    
                    elif mask_M_HD[coordinates] != False:
                        AS_weight = cac.AS_weight_calc(mask_M_HD, CCI_array[:,:,slice_index], C.calcium_threshold, self.header.ImageType)
                        self.calc_size_density_VS_AS_MS['Medium_HD'][2] += AS_weight * area
    
                        self.calc_size_density_VS_AS_MS['Medium_HD'][3] += MS                    
                        calc_mean_dict = cac.calc_mean_calculator(calc_mean_dict, 'Medium_HD', output[1].copy(),\
                                            CCI_array[:,:,slice_index].copy(), slice_index2)
                    
                    elif mask_M_MD[coordinates] != False:
                        AS_weight = cac.AS_weight_calc(mask_M_MD, CCI_array[:,:,slice_index], C.calcium_threshold, self.header.ImageType)
                        self.calc_size_density_VS_AS_MS['Medium_MD'][2] += AS_weight * area
                        
                        self.calc_size_density_VS_AS_MS['Medium_MD'][3] += MS                     
                        calc_mean_dict = cac.calc_mean_calculator(calc_mean_dict, 'Medium_MD', output[1].copy(),\
                                            CCI_array[:,:,slice_index].copy(), slice_index2)
                   
                    elif mask_M_LD[coordinates] != False:
                        AS_weight = cac.AS_weight_calc(mask_M_LD, CCI_array[:,:,slice_index], C.calcium_threshold, self.header.ImageType)
                        self.calc_size_density_VS_AS_MS['Medium_LD'][2] += AS_weight * area
                        
                        self.calc_size_density_VS_AS_MS['Medium_LD'][3] += MS  
                        
                        calc_mean_dict = cac.calc_mean_calculator(calc_mean_dict, 'Medium_LD', output[1].copy(),\
                                            CCI_array[:,:,slice_index].copy(), slice_index2)
                    
                    elif mask_S_HD[coordinates] != False:
                        AS_weight = cac.AS_weight_calc(mask_S_HD, CCI_array[:,:,slice_index], C.calcium_threshold, self.header.ImageType)
                        self.calc_size_density_VS_AS_MS['Small_HD'][2] += AS_weight * area
    
                        self.calc_size_density_VS_AS_MS['Small_HD'][3] += MS 
                        calc_mean_dict = cac.calc_mean_calculator(calc_mean_dict, 'Small_HD', output[1].copy(),\
                                            CCI_array[:,:,slice_index].copy(), slice_index2)
                    
                    elif mask_S_MD[coordinates] != False:
                        AS_weight = cac.AS_weight_calc(mask_S_MD, CCI_array[:,:,slice_index], C.calcium_threshold, self.header.ImageType)
                        self.calc_size_density_VS_AS_MS['Small_MD'][2] += AS_weight * area
                        
                        self.calc_size_density_VS_AS_MS['Small_MD'][3] += MS
                        calc_mean_dict = cac.calc_mean_calculator(calc_mean_dict, 'Small_MD', output[1].copy(),\
                                            CCI_array[:,:,slice_index].copy(), slice_index2)
                    
                    elif mask_S_LD[coordinates] != False:
                        AS_weight = cac.AS_weight_calc(mask_S_LD, CCI_array[:,:,slice_index], C.calcium_threshold, self.header.ImageType)
                        self.calc_size_density_VS_AS_MS['Small_LD'][2] += AS_weight * area
    
                        self.calc_size_density_VS_AS_MS['Small_LD'][3] += MS
                        calc_mean_dict = cac.calc_mean_calculator(calc_mean_dict, 'Small_LD', output[1].copy(),\
                                            CCI_array[:,:,slice_index].copy(), slice_index2)
                    else:
                        pass
                else:
                    pass
          
        if (self.header.Manufacturer == 'PHILIPS' or self.header.Manufacturer == 'CANON' or self.header.Manufacturer == 'GE'):
            for key in self.calc_size_density_VS_AS_MS.keys():
                self.calc_size_density_VS_AS_MS[key][1] = 0
                self.calc_size_density_VS_AS_MS[key][3] = 0
            
            min_area = cac.min_area_det(self.header.Manufacturer)
            
            CCI_array_binary = CCI_array.copy()
            if self.header.Manufacturer == 'PHILIPS':
                CCI_array_binary[CCI_array_binary < C.Philips_mass_score_threshold] = 0
            else:
                CCI_array_binary[CCI_array_binary < self.calcium_threshold] = 0
            CCI_array_binary[CCI_array_binary > 0] = 1
            CCI_array_binary = CCI_array_binary.astype(dtype = np.uint8)
        
                 
            # Volume and Mass score calculation
            for slice_index in range(CCI_array.shape[2]):
                output = pat.arr_connected_components(CCI_array_binary[:,:,slice_index])
                
                for slice_index2 in range(1,output[0]):
                    coordinates = int(output[3][slice_index2][1]), int(output[3][slice_index2][0])
                    area = output[2][slice_index2][4] * self.header.PixelSpacing[0]**2 
                    
                    mask_mean_mass = output[1].copy()
                    mask_mean_mass[mask_mean_mass != slice_index2] = 0
                    mask_mean_mass[mask_mean_mass != 0] = 1
    
                    if area > min_area:
                        MS = area * self.header.SliceThickness * cac.mass_mean_def(mask_mean_mass * CCI_array[:,:,slice_index], self.calcium_threshold)
                        VS = area * self.header.SliceThickness
                        
                        if mask_L_HD[coordinates] != False:
                            self.calc_size_density_VS_AS_MS['Large_HD'][1] += VS
                            self.calc_size_density_VS_AS_MS['Large_HD'][3] += MS 
                        elif mask_L_MD[coordinates] != False:
                            self.calc_size_density_VS_AS_MS['Large_MD'][1] += VS
                            self.calc_size_density_VS_AS_MS['Large_MD'][3] += MS  
                        
                        elif mask_L_LD[coordinates] != False:
                            self.calc_size_density_VS_AS_MS['Large_LD'][1] += VS
                            self.calc_size_density_VS_AS_MS['Large_LD'][3] += MS         
                        
                        elif mask_M_HD[coordinates] != False:
                            self.calc_size_density_VS_AS_MS['Medium_HD'][1] += VS
                            self.calc_size_density_VS_AS_MS['Medium_HD'][3] += MS  
                        
                        elif mask_M_MD[coordinates] != False:
                            self.calc_size_density_VS_AS_MS['Medium_MD'][1] += VS
                            self.calc_size_density_VS_AS_MS['Medium_MD'][3] += MS  
                       
                        elif mask_M_LD[coordinates] != False:
                            self.calc_size_density_VS_AS_MS['Medium_LD'][1] += VS
                            self.calc_size_density_VS_AS_MS['Medium_LD'][3] += MS               
                        
                        elif mask_S_HD[coordinates] != False:
                            self.calc_size_density_VS_AS_MS['Small_HD'][1] += VS
                            self.calc_size_density_VS_AS_MS['Small_HD'][3] += MS  
                        
                        elif mask_S_MD[coordinates] != False:
                            self.calc_size_density_VS_AS_MS['Small_MD'][1] += VS
                            self.calc_size_density_VS_AS_MS['Small_MD'][3] += MS   
                        
                        elif mask_S_LD[coordinates] != False:
                            self.calc_size_density_VS_AS_MS['Small_LD'][1] += VS
                            self.calc_size_density_VS_AS_MS['Small_LD'][3] += MS  
                        else:
                            pass
                    else:
                        pass
           
        for key in self.calc_size_density_VS_AS_MS.keys():
            self.calc_size_density_VS_AS_MS[key][1] = round(self.calc_size_density_VS_AS_MS[key][1],5)
            self.calc_size_density_VS_AS_MS[key][2] = round(self.calc_size_density_VS_AS_MS[key][2] * self.header.SliceThickness / 3,5)
            self.calc_size_density_VS_AS_MS[key][3] = round(self.calc_size_density_VS_AS_MS[key][3] * self.mass_cal_factor,5)
        
        for key in calc_mean_dict.keys():
            calc_mean_dict[key] = round(calc_mean_dict[key][0] / calc_mean_dict[key][1],5)
            
        self.calc_mean_dict = calc_mean_dict
            
        # return calc_size_density_VS_AS_MS, center, mass_cal_factor, calc_mean_dict, angle_0_200HA, water_rod_metrics, SNR_dict, BAS_Mask
        return None
    
    
    def calcium_quality_CCI(self, ROI_size = 55):
        # Function to calculate QC        
        array = self.array[:,:,self.quality_slice].copy()
                                                        
        quality_size = int((ROI_size / self.pixel_size) / 2)
        
        dcm_quality = array[int(self.center_insert[0] - quality_size):int(self.center_insert[0] + quality_size),\
                                int(self.center_insert[1] - quality_size):int(self.center_insert[1] + quality_size)]
        
        qc = {}
        qc['mean'] = round(dcm_quality.mean(),5)
        qc['SD'] = round(dcm_quality.std(),5)
        
        array_BAS_CCI = self.array[:,:,self.slice_CCI].copy()
        array_BAS_CCI = array_BAS_CCI * self.BAS_Mask
        
        array_binary_BAS = array_BAS_CCI.copy()
        array_binary_BAS[array_binary_BAS < self.calcium_threshold] = 0
        array_binary_BAS[array_binary_BAS > 0] = 1
        array_binary_BAS = array_binary_BAS.astype(dtype = np.uint8)
        
        output_BAS = cv2.connectedComponentsWithStats(array_binary_BAS, C.comp_connect, cv2.CV_32S)
        qc_areas_BAS = []
        for index2 in range(output_BAS[0]):
            qc_areas_BAS.append(output_BAS[2][index2][4])
            
        qc_max_area_BAS = qc_areas_BAS.index(max(qc_areas_BAS))
        
        AS_slice = 0        
        for component_index in range(1,output_BAS[0]):
            if component_index != qc_max_area_BAS:
                tmp_array = output_BAS[1].copy()
                tmp_array[tmp_array != component_index] = 0
                tmp_array = tmp_array * array_BAS_CCI
    
                area = output_BAS[2][component_index][4] * self.pixel_size**2
                
                min_area = cac.min_area_det(self.header.Manufacturer, self.pixel_size)
        
                if area > min_area:
                    AS_weight = cac.AS_weight_calc(np.ones_like(tmp_array), tmp_array, C.calcium_threshold, self.header.ImageType)
                    AS = area * AS_weight
                    AS_slice += AS       
                
                
        qc['AS'] = round(AS_slice,5)
                   
            
        # Calculate NPS
        qc['NPS'] = pat.nps_calc(array, self.pixel_size, self.center_insert,\
                                 num_ROIs = 18, rad = 32, ROI_size = 15, plot_NPS = False)
        
        # Calculate NTD
        num_out_3SD = 0
        for voxel in array.flatten():
            test_value = abs(voxel - qc['mean'])
            if test_value >= (qc['SD'] * 3):
                num_out_3SD += 1
        qc['NTD'] = num_out_3SD / (array.shape[0] * array.shape[1])
            
        # Calculate NPS peak
        qc['NPS_peak'] = qc['NPS'][0][np.where(qc['NPS'][1] == (qc['NPS'][1].max()))[0][0]]

        self.qc = qc
        return None
    
    
    @classmethod
    def AS_200mgHA(cls, array_200mgHA, mask_used, threshold_used, imType, pixelsize):
        array_200mgHA_original = array_200mgHA.copy()
        array_200mgHA_binary = array_200mgHA_original.copy()
        array_200mgHA_binary[array_200mgHA_binary < threshold_used] = 0
        array_200mgHA_binary[array_200mgHA_binary > 0] = 1
        array_200mgHA_binary = array_200mgHA_binary.astype(dtype = np.uint8)
        
        array_200mgHA_binary = pat.dcm_filtering(array_200mgHA_binary, 7)
    
        output = cv2.connectedComponentsWithStats(array_200mgHA_binary, C.comp_connect, cv2.CV_32S)
        
        tmp_size_dict = {}
        for index in range(1,output[0]):
            tmp_size_dict[index] = output[2][index][4]
        
        _, comp_index = max(zip(tmp_size_dict.values(), tmp_size_dict.keys()))
        
        if mask_used:
            mask = pat.create_circular_mask(cls.header.Columns, cls.header.Rows,\
                                    output[3][comp_index], int(6/ cls.pixel_size))
        
            array_200mgHA_binary    = mask * array_200mgHA_binary
            array_200mgHA_original  = mask * array_200mgHA_original
            
            output = cv2.connectedComponentsWithStats(array_200mgHA_binary, C.comp_connect, cv2.CV_32S)
        
        cal_rod_array = output[1]
        cal_rod_array[cal_rod_array!=comp_index] = 0
        cal_rod_array[cal_rod_array!=0] = 1
        
        AS_weight = cac.AS_weight_calc(cal_rod_array, array_200mgHA, threshold_used, imType)
        AS_calrod = round(AS_weight * cal_rod_array.sum() * pixelsize**2,1)
        
        mean_calrod = round((cal_rod_array * array_200mgHA_original).sum() / np.count_nonzero(cal_rod_array),1)
        return AS_calrod, mean_calrod

    
    def Measurement_calrod(self, masked_values = False):
        array_200mgHA = self.calcium_image[:,:,self.cal_rod_slice].copy()
        # calculate mean and AS for calibration rod    
        AS_VarHU_tot, mean_VarHU_tot = self.AS_200mgHA(array_200mgHA, False, self.calcium_threshold, self.header.ImageType, self.pixel_size)
        AS_130HU_tot, mean_130HU_tot = self.AS_200mgHA(array_200mgHA, False, 130, self.header.ImageType, self.pixel_size)
        if masked_values:
            AS_130HU_small, mean_130HU_small = self.AS_200mgHA(array_200mgHA, True, self.calcium_threshold, self.header.ImageType, self.pixel_size)
            AS_VarHU_small, mean_VarHU_small = self.AS_200mgHA(array_200mgHA, True, 130, self.header.ImageType, self.pixel_size)
        else:
            AS_130HU_small, mean_130HU_small = None, None
            AS_VarHU_small, mean_VarHU_small = None, None
        
        Agatston_cal_rod    = [AS_130HU_small, AS_VarHU_small, AS_130HU_tot, AS_VarHU_tot]
        Mean_cal_rod        = [mean_130HU_small, mean_VarHU_small, mean_130HU_tot, mean_VarHU_tot]
        
        
        self.Cal_rod_AS = Agatston_cal_rod
        self.Cal_rod_mean = Mean_cal_rod
        
        return None
    
    
    @staticmethod
    def ESF_logistic_one_function(ESF, mean_bottom, mean_top, header, factor, int_pix_sp, plot = False):
        def Fermi_func2(x, d, a, b, c):
            return d + (a / (np.exp((x - b)/c) + 1))
    
        A = mean_bottom
        B = mean_top
        R = len(ESF) / 2
        d = A
        a = 0.75*(B-A)
        b = R
        c = 0.15
        
        pixels = np.arange(0,len(ESF))
        
        params, params_covariance = optimize.curve_fit(Fermi_func2, pixels, ESF,
                                                    p0=[d, a, b, c],\
                                                        method = 'lm', maxfev = 50000)
            
        ESF = []
        for element in pixels:
            output = Fermi_func2(element, params[0], params[1], params[2], params[3])
            # print(element, output)
            ESF.append(output)
            
        ESF_eq = str(params[0]) + '+(' + str(params[1]) + '/(exp((x-' + str(params[2]) + ')/' + str(params[3]) + ')+1))'
        
        x = sympy.Symbol('x')
        y = sympy.Symbol('y')
        esf = params[0]+(params[1]/(sympy.exp((x-params[2])/params[3])+1))
        lsf = -(params[1] * sympy.exp((x - params[2]) / params[3]) /\
                (params[3] * (sympy.exp((x - params[2]) / params[3]) + 1)**2))
    
        lsf_fwhm = params[3] * sympy.ln(-y / ((y * params[3] * sympy.exp(2)) + (2 * y) +
                                              params[1])) + params[2]
        
        lsf_fwhm = params[3] * sympy.ln((sympy.sqrt(params[1]) * sympy.sqrt(sympy.exp(params[2] / params[3])) *\
                                        sympy.sqrt(params[1] * sympy.exp(params[2] / params[3]) + 4 * params[3] * y *\
                                                   sympy.exp(params[2] / params[3])) + params[1] * -sympy.exp(params[2] / params[3]) -\
                                            2* params[3] * y * sympy.exp(params[2] / params[3])) / ( 2 * params[3] * y))
    
        # blooming_image = sympy.integrate(esf, (x,0,200))
        
        # print(blooming_image)
        
        LSF_eq = esf.diff(x)
        if plot:
            sympy.plotting.plot(esf, (x, 0, len(ESF)))
            sympy.plotting.plot(lsf, (x, 0, len(ESF)))
    
        LSF_max = []
        voxels = np.arange(0,len(ESF),.1)
        for voxel in voxels:
            LSF_max.append(LSF_eq.subs(x, voxel))
            
        LSF_max_x = LSF_max.index(max(LSF_max)) / 10
            
        LSF_HM = max(LSF_max) / 2
        LSF_x_HM = lsf_fwhm.subs(y, LSF_HM).evalf()
        
        LSF_FWHM = 2 * abs(LSF_max_x - LSF_x_HM) * int_pix_sp
               
        LSF_eq = str(LSF_eq)
        
    
        return ESF, pixels, ESF_eq, LSF_eq, LSF_FWHM

    
    def MTF_calc(self, plot_MTF = False):
        select_slice = self.slice_CCI + (self.flipped * int(35 / self.header.SliceThickness))
        factor = 6
        interpol_pixelspacing = self.pixel_size / factor
    
        array_int = pat.interp_2D(self.array[:,:,select_slice], factor, interp_type = 'linear').copy()
        array_binary = array_int.copy()
        array_binary[array_binary < 1.2* self.calcium_threshold] = 0
        array_binary[array_binary > 0] = 1
        array_binary = array_binary.astype(dtype = np.uint8)    
        
        kern = math.ceil(5 / self.pixel_size)
        if kern % 2 == 0:
            kern += 1
            
        array_binary = pat.dcm_filtering(array_binary, image_kernel = kern)
        
        # plt.imshow(array_binary)
        # plt.show()
    
        output = cv2.connectedComponentsWithStats(array_binary, 4, cv2.CV_32S)
        sizes = []
        for index in range(1,output[0]):
            sizes.append(output[2][index][4])
            
        center_index = sizes.index(max(sizes)) + 1
    
        center_updated = [int(output[3][center_index][1]), int(output[3][center_index][0])]
    
        
        x_y_size = math.ceil(21 / interpol_pixelspacing)
        array_200mgHA = np.zeros([x_y_size * 2 + 1, x_y_size* 2 + 1])
    
        array_200mgHA = array_int[center_updated[0] - x_y_size:center_updated[0] + x_y_size + 1,\
                                   center_updated[1] - x_y_size:center_updated[1] + x_y_size + 1]
            
        # plt.imshow(array_200mgHA)
        # plt.show()
            
        angles = list(range(360))
        non_used_angles = list(range(int(self.angle_0_200HA - 45), int(self.angle_0_200HA + 45)))
        used_angles = [used_angle for used_angle in angles if used_angle not in non_used_angles]    
        ESF = [0]
        for index in used_angles:
            tmp_array = array_200mgHA.copy()
            tmp_array = rotate(tmp_array,index)
            new_ESF = tmp_array[x_y_size + 1,0:x_y_size + 1]
            if len(ESF) == 1:
                ESF = new_ESF
            else:
                ESF += new_ESF
        ESF = ESF / len(used_angles)    
        #ESF, ESF_x = ESF_logistic(ESF, quality_output['mean'], Cal_rod_mean[2], header, factor)
        self.qc['ESF'], ESF_x, self.qc['ESF_eq'], self.qc['LSF_eq'], self.qc['LSF_FWHM'] =\
            self.ESF_logistic_one_function(ESF, self.qc['mean'], self.Cal_rod_mean[2], self.header, factor, interpol_pixelspacing, plot = False)
        
        # plt.scatter(ESF_x, ESF)
        # plt.show()
        
        LSF = np.abs(np.diff(ESF))
        LSF = LSF*np.hanning(len(LSF))
        
        self.qc['LSF'] = LSF
        
        # quality_output['LSF_FWHM'] = calc_LSF_FWHM(LSF, interpol_pixelspacing)
    
        MTF = np.abs(np.fft.fft(LSF)) / sum(LSF)
        
        MTF_y = MTF[:int(len(LSF)/2)]
        MTF_x = np.linspace(0, 1 / interpol_pixelspacing, len(LSF))
        MTF_x = MTF_x[:int(len(LSF)/2)] 
        
        MTF = [MTF_x, MTF_y]
        
        MTF_0_5 = round(np.interp(0.5, np.flip(MTF_y,0), np.flip(MTF_x,0)),2)
        MTF_0_1 = round(np.interp(0.1, np.flip(MTF_y,0), np.flip(MTF_x,0)),2)
        
        if plot_MTF:
            plt.plot(MTF[0], MTF[1])
            plt.xlim(0,1.2)
            plt.ylim(0,1)
            plt.show()
            print("\nMTF_0.5 = ", MTF_0_5)
            print("MTF_0.1 = ", MTF_0_1)
            
        self.qc['MTF'] = MTF 
        self.qc['MTF0.5'] = MTF_0_5    
        
        return None
            
   
