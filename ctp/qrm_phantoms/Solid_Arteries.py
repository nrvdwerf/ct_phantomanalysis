# main document for solid_artery analysis

import pydicom
# import SimpleITK as sitk
import os
import numpy as np
import scipy
import matplotlib.pyplot as plt
import cv2
import math
import statistics
from ctp.Phantom_analysis_toolbox import Phantom_analysis_toolbox as pat
from ctp.Phantom_analysis_toolbox import CAC_scoring as cac
from ctp.qrm_phantoms.Thorax import QRM_thorax_analyser as thorax
import constants as C
from skimage.transform import rotate
from scipy import optimize
import sympy
from sys import platform
  


class Solid_Artery_analyser(thorax):
    def __init__(self, used_path):
        super().__init__(used_path)
        
        self.int_factor = self.header.SliceThickness / self.pixel_size
        self.arteries_masked()
        if (C.Volume_score_calc and ('SIEMENS' in self.header.Manufacturer.upper()  or self.header.Manufacturer == 'Literature')):
            self.int_header, self.int_array = pat.dcm_interpolation(self.array, self.header)
        
        self.arteries_calcium_image()
        self.mass_cal_factor = 0.001
        self.mask_scoring_def()
        self.artery_scoring()
        self.arteries_quality()
        
        return None
    
    
    def arteries_masked(self, radius_val = 80):
        radius = (radius_val/2) / self.pixel_size
        
        Y, X = np.ogrid[:self.header.Rows, :self.header.Columns]
        dist_from_center = np.sqrt((X - self.center_insert[1])**2 + (Y- self.center_insert[0])**2)
    
        mask = dist_from_center <= radius  
        masked_array = np.zeros_like(self.array)
        for index in range(self.array.shape[2]):
            masked_array[:,:,index] = self.array[:,:,index] * mask
            
        self.array = masked_array
        self.mask = mask
    
        return None
    
    
    @staticmethod
    def find_CAC_in_arteries(dcm_array, fact, slice_dict, header, threshold):
        while (fact > 0.9 and len(slice_dict) == 0):
            image_kernel = 5
            while (image_kernel < 9 and len(slice_dict) == 0):
                array = dcm_array.copy()
                array[array < fact*threshold] = 0
                array[array > 0] = 1
                array = array.astype(dtype = np.uint8)
            
                artery_num_pixels = math.ceil(math.pi * (5/2)**2 / header.PixelSpacing[0]**2)
                
                for idx in range(math.ceil(5/header.SliceThickness), array.shape[2] - math.ceil(5/header.SliceThickness)):
                    array_filtered = scipy.signal.medfilt2d(array[:,:,idx], image_kernel)
                    # plt.imshow(array_filtered)
                    # plt.show()
                    
                    output = cv2.connectedComponentsWithStats(array_filtered, 4, cv2.CV_32S)
                    count_5mm = 0
                    count_large = 0
                    count_small = 0
                        
                    for index in range(1,output[0]):
                        tmp_size_calc = output[2][index][4]
                        if tmp_size_calc < artery_num_pixels * 0.3:
                            count_small += 1
                        elif tmp_size_calc > artery_num_pixels * 4:
                            count_large += 1
                        else:
                            count_5mm += 1
                            
                    # print(idx, count_small, count_large, count_5mm)
                    
                    if (count_5mm == 1 and count_large == 0 and count_small < 3):
                        slice_dict[idx] = count_5mm
                        # plt.imshow(array_filtered)
                        # plt.title(idx)
                        # plt.show()
                image_kernel += 2
            fact -= 0.1
            
        threshold_kernel_central = [fact * threshold, image_kernel]
           
        return slice_dict, array, threshold_kernel_central
    
    @staticmethod
    def calcification_center(array, calc_indices):
        calc_mid = calc_indices[0] + math.ceil((calc_indices[1] - calc_indices[0])/2)
        
        array_filtered = scipy.signal.medfilt2d(array[:,:,calc_mid], 5)
        output = cv2.connectedComponentsWithStats(array_filtered, 4, cv2.CV_32S)
        
        center_calc = [int(output[3][1][0]), int(output[3][1][1])] 
        return center_calc


    
    def arteries_calcium_image(self):
        # threshold array according to calcium_threshold 
        # simultaneously make image binary
        fact = 1.2
        slice_dict = {}
        threshold_used = self.calcium_threshold
        
        if (C.calcium_threshold == 'monoE' and threshold_used < 130):
            threshold_used = 110
        else:
            pass
        
        slice_dict, array, threshold_kernel_central = self.find_CAC_in_arteries(self.array, fact, slice_dict, self.header, threshold_used)
        
        if (C.calcium_threshold == 'monoE' and self.calcium_threshold < 130 and len(slice_dict) == 0):
            slice_dict, array, threshold_kernel_central = self.find_CAC_in_arteries(self.array, fact, slice_dict, self.header, 70)
        else:
            pass
            
        # print(slice_dict, threshold_used)
        
        slice_dict_ordered = {}
        for index in range(self.array.shape[2]):
            if index in slice_dict.keys():
                slice_dict_ordered[index] = slice_dict[index]
            else:
                pass    
        
        min_key = []
        max_key = []
        for key in slice_dict_ordered.keys():
            # print(key, slice_dict_ordered[key])
            if (key - 1) not in list(slice_dict_ordered.keys()):
               min_key.append(key) 
            if (key + 1) not in list(slice_dict_ordered.keys()):
               max_key.append(key)
               
        # print(slice_dict_ordered)
        # print(min_key, max_key)
        
        
        #calc1 should be the higher density calc
        calc1 = [min_key[0], max_key[0]]
        calc1.append(self.calcification_center(array, calc1))
            
        if len(min_key) == 1:
            calc2 = [calc1[0] + int(30 / self.header.SliceThickness), calc1[1] + int(30 / self.header.SliceThickness)]
            calc2.append(self.calcification_center(array, calc1))
        elif (abs(min_key[1] - min_key[0]) * self.header.SliceThickness) > 35:
            calc2 = [calc1[0] + int(30 / self.header.SliceThickness), calc1[1] + int(30 / self.header.SliceThickness)]
            calc2.append(self.calcification_center(array, calc1))
        elif (abs(min_key[1] - min_key[0]) * self.header.SliceThickness) < 18:
            calc2 = [calc1[0] + int(30 / self.header.SliceThickness), calc1[1] + int(30 / self.header.SliceThickness)]
            calc2.append(self.calcification_center(array, calc1))
        else:
            calc2 = [min_key[1], max_key[1]]
            calc2.append(self.calcification_center(array, calc2))
            
        # create calcium image
        array = self.array.copy()
        array[array < self.calcium_threshold] = 0
        array[array > 0] = 1
        array = array.astype(dtype = np.uint8)
        self.calcium_image = array * self.array
        self.calc1 = calc1
        self.calc2 = calc2
        self.threshold_kernel_central = threshold_kernel_central
        
        return None
    
    
    def mask_scoring_def(self):
        calc1_max_tmp = self.calcium_image[:,:, int(self.calc1[1] - (self.calc1[1] - self.calc1[0]) / 2)].max()
        calc2_max_tmp = self.calcium_image[:,:, int(self.calc2[1] - (self.calc2[1] - self.calc2[0]) / 2)].max()                            
        
        if calc1_max_tmp > calc2_max_tmp:
            array = self.calcium_image[:,:, int(self.calc1[1] - (self.calc1[1] - self.calc1[0]) / 2)].copy()
        else:
            array = self.calcium_image[:,:, int(self.calc2[1] - (self.calc2[1] - self.calc2[0]) / 2)].copy()                          
        
        array[array < self.threshold_kernel_central[0]] = 0
        array[array > 0] = 1
        array = array.astype(dtype = np.uint8)
        
        array = scipy.signal.medfilt2d(array, self.threshold_kernel_central[1])
        
        mask_area = np.zeros([array.shape[0], array.shape[1]])
    
        output = cv2.connectedComponentsWithStats(array, C.comp_connect, cv2.CV_32S)
    
        center_mask = [output[3][1][0], output[3][1][1]]
    
        
        mask_area = pat.create_circular_mask(array.shape[0], array.shape[1], center_mask, 8 / self.pixel_size)
        
        # plt.imshow(array - mask_area)
        # plt.show()
        
        self.mask_scoring = mask_area
        return None
    
    

    def arteries_scoring(self, calc_index):
        # Actual scoring for arteries insert per calcification         
        calc_min = calc_index[0] - math.ceil(5 / self.header.SliceThickness)
        calc_max = calc_index[1] + math.ceil(5 / self.header.SliceThickness) 
        if calc_min < 0:
            calc_min = 0
        if calc_max > self.array.shape[2]:
            calc_max = self.array.shape[2]
            
            
        calc_array = self.array[:,:,calc_min:calc_max]
        calcium_calc_array = self.calcium_image[:,:,calc_min:calc_max]
        
        calc_array_binary = calc_array.copy()
        calc_array_binary[calc_array_binary < self.calcium_threshold] = 0
        calc_array_binary[calc_array_binary > 0] = 1
        calc_array_binary = calc_array_binary.astype(dtype = np.uint8)
    
        if (C.Volume_score_calc and ('SIEMENS' in self.header.Manufacturer.upper()  or self.header.Manufacturer == 'Literature')): 
            calc_int_min = int(calc_min * self.int_factor + self.int_factor / 2)
            calc_int_max = int(calc_max * self.int_factor - self.int_factor / 2)
            calc_array_int = self.int_array[:,:,calc_int_min:calc_int_max]
            
            calc_array_int_binary = calc_array_int.copy()
            calc_array_int_binary[calc_array_int_binary < self.calcium_threshold] = 0
            calc_array_int_binary[calc_array_int_binary > 0] = 1
            calc_array_int_binary = calc_array_int_binary.astype(dtype = np.uint8)
            
            # Volume score calculation
            Volume_score = 0
            for slice_index in range(calc_array_int.shape[2]):
                output = cv2.connectedComponentsWithStats(calc_array_int_binary[:,:,slice_index],\
                                                              C.comp_connect, cv2.CV_32S)
        
                for slice_index2 in range(1,output[0]):
                    coordinates = int(output[3][slice_index2][1]), int(output[3][slice_index2][0])
                    area = output[2][slice_index2][4] * self.int_header.PixelSpacing[0]**2
                    
                    min_area = cac.min_area_det(self.header.Manufacturer, self.pixel_size)
                    
                    if area > min_area:
                        VS = area * self.int_header.SliceThickness
                        if self.mask_scoring[coordinates] != False:
                            Volume_score += VS
                        else:
                            pass
                    else:
                        pass
        else:
            Volume_score = 0
              
        # Agatston and Mass score calculation
        Agatston_score = 0
        Mass_score = 0
        num_voxels = 0
        mean_calc_tmp = 0
        max_calc_tmp = []
        for slice_index in range(calc_array.shape[2]):
            output = cv2.connectedComponentsWithStats(calc_array_binary[:,:,slice_index],\
                                                          C.comp_connect, cv2.CV_32S)
                
            for slice_index2 in range(1,output[0]):
                coordinates = int(output[3][slice_index2][1]), int(output[3][slice_index2][0])
                area = output[2][slice_index2][4] * self.header.PixelSpacing[0]**2
                num_vox_calc = output[2][slice_index2][4]
                   
    
                min_area = cac.min_area_det(self.header.Manufacturer, self.pixel_size)
                        
                if area > min_area:
                    if self.mask_scoring[coordinates] != False:
                        AS_weight = cac.AS_weight_calc(self.mask_scoring, calcium_calc_array[:,:,slice_index], C.calcium_threshold, self.header.ImageType)
                        Agatston_score += AS_weight * area
                        
                        MS = area * self.header.SliceThickness * cac.mass_mean_def(self.mask_scoring * calcium_calc_array[:,:,slice_index], self.calcium_threshold)
                        Mass_score += MS
                        
                        num_voxels += num_vox_calc
    
                        tmp_mean_array = output[1].copy()
                        tmp_mean_array[tmp_mean_array != slice_index2] = 0
                        tmp_mean_array[tmp_mean_array != 0] = 1
                        tmp_mean_array = tmp_mean_array * self.mask_scoring
                        tmp_mean_array = tmp_mean_array * calcium_calc_array[:,:,slice_index]
                        tmp_mean_array[tmp_mean_array < self.calcium_threshold] = 0
                        mean_calc_tmp += tmp_mean_array.sum()
                        max_calc_tmp.append(tmp_mean_array.max())
                    else:
                        pass
                else:
                    pass  
    
        if (self.header.Manufacturer.upper() == 'PHILIPS' or self.header.Manufacturer.upper() == 'CANON'):
            Mass_score = 0
            Volume_score = 0
            num_voxels = 0
            mean_calc_tmp = 0
            
            min_area = cac.min_area_det(self.header.Manufacturer, self.pixel_size)
            
            calc_array_binary = calc_array.copy()
            if self.header.Manufacturer == 'PHILIPS':
                calc_array_binary[calc_array_binary < C.Philips_mass_score_threshold] = 0
            else:
                calc_array_binary[calc_array_binary < self.calcium_threshold] = 0
            calc_array_binary[calc_array_binary > 0] = 1
            calc_array_binary = calc_array_binary.astype(dtype = np.uint8)
            
            # Volume and Mass score calculation
            for slice_index in range(calc_array.shape[2]):
                output = cv2.connectedComponentsWithStats(calc_array_binary[:,:,slice_index],\
                                                              C.comp_connect, cv2.CV_32S)
                
                for slice_index2 in range(1,output[0]):
                    coordinates = int(output[3][slice_index2][1]), int(output[3][slice_index2][0])
                    area = output[2][slice_index2][4] * self.pixel_size**2 
                    num_vox_calc = output[2][slice_index2][4]
                    
                    mask_mean_mass = output[1].copy()
                    mask_mean_mass[mask_mean_mass != slice_index2] = 0
                    mask_mean_mass[mask_mean_mass != 0] = 1
    
                    if area > min_area:
                        MS = area * self.header.SliceThickness * cac.mass_mean_def(mask_mean_mass * calcium_calc_array[:,:,slice_index], self.calcium_threshold)
                        VS = area * self.header.SliceThickness
                        
                        
                        if self.mask_scoring[coordinates] != False:
                            MS = area * self.header.SliceThickness * cac.mass_mean_def(self.mask_scoring * calcium_calc_array[:,:,slice_index], self.calcium_threshold)
                            VS = area * self.header.SliceThickness
                            
                            Mass_score += MS 
                            Volume_score += VS
                            
                            num_voxels += num_vox_calc
                            
                            tmp_mean_array = output[1].copy()
                            tmp_mean_array[tmp_mean_array != slice_index2] = 0
                            tmp_mean_array[tmp_mean_array != 0] = 1
                            tmp_mean_array = tmp_mean_array * self.mask_scoring
                            tmp_mean_array = tmp_mean_array * calcium_calc_array[:,:,slice_index]
                            tmp_mean_array[tmp_mean_array < self.calcium_threshold] = 0
                            mean_calc_tmp += tmp_mean_array.sum()
                    else:
                        pass      
                
        Volume_score = round(Volume_score, 5)
        Mass_score = round(Mass_score * self.mass_cal_factor, 5)
        Agatston_score = round(Agatston_score * self.header.SliceThickness / 3, 5)
        
        scores = [Agatston_score, Mass_score, Volume_score]
    
        return scores, mean_calc_tmp, num_voxels, max_calc_tmp
    
    
    def artery_scoring(self):
        self.scores_calc1, self.calc1_mean, self.calc1_numvox, self.max_calc1 = self.arteries_scoring(self.calc1)
        self.scores_calc2, self.calc2_mean, self.calc2_numvox, self.max_calc2 = self.arteries_scoring(self.calc2)
        
        self.max_calc1 = max(self.max_calc1)
        self.max_calc2 = max(self.max_calc2)
            
        return None
    
    
    def arteries_quality(self):        
        array = self.array[:,:,int((self.calc1[1] + self.calc2[0]) / 2)].copy()
        array = array * (1 - self.mask_scoring)
        ROI_size = 50                                               
                                                        
        quality_size = int((ROI_size / self.pixel_size) / 2)
        
        dcm_quality = array[int(self.center_insert[0] - quality_size):int(self.center_insert[0] + quality_size),\
                                int(self.center_insert[1] - quality_size):int(self.center_insert[1] + quality_size)]
        
        qc = {}
        qc['mean'] = round(dcm_quality.mean(),5)
        qc['SD'] = round(dcm_quality.std(),5)
        
        qc['mean'] = dcm_quality.sum() / np.count_nonzero(dcm_quality)
        tmp_SD = dcm_quality.copy()
        tmp_SD[tmp_SD != 0] -= qc['mean']
        qc['SD'] = math.sqrt((tmp_SD**2).sum() /\
                             np.count_nonzero(dcm_quality))
     
        array_binary = dcm_quality.copy()
        array_binary[array_binary < self.calcium_threshold] = 0
        array_binary[array_binary > 0] = 1
        array_binary = array_binary.astype(dtype = np.uint8)
        
        output = cv2.connectedComponentsWithStats(array_binary, C.comp_connect, cv2.CV_32S)
        qc_areas = []
        for index in range(output[0]):
            qc_areas.append(output[2][index][4])
            
        qc_max_area = qc_areas.index(max(qc_areas))
        
        AS_slice = 0        
        for component_index in range(1,output[0]):
            if component_index != qc_max_area:
                tmp_array = output[1].copy()
                tmp_array[tmp_array != component_index] = 0
                tmp_array = tmp_array * dcm_quality
    
                area = output[2][component_index][4] * self.pixel_size**2
                
                min_area = cac.min_area_det(self.header.Manufacturer, self.pixel_size)
        
                if area > min_area:
                    AS_weight = cac.AS_weight_calc(np.ones_like(tmp_array), tmp_array, C.calcium_threshold, self.header.ImageType)
                    AS = area * AS_weight
                    AS_slice += AS                 
            
        qc['AS'] = round(AS_slice,5)
        
        # Calculate NPS
        qc['NPS'] = pat.nps_calc(self.array_ori[:,:,int((self.calc1[1] + self.calc2[0]) / 2)].copy(), self.pixel_size, self.center_insert,\
                                 num_ROIs = 18, rad = 32, ROI_size = 15, plot_NPS = False)
            
        
        if self.calc1_numvox != 0:
            qc['mean_calc1'] = round(self.calc1_mean / self.calc1_numvox,5) 
            qc['SNR_calc1'] = round(qc['mean_calc1'] / qc['SD'],5)
            qc['CNR_calc1'] = abs(qc['mean_calc1'] - qc['mean']) / qc['SD']
        else:
            qc['mean_calc1'] = 0
            qc['SNR_calc1'] = 0
            qc['CNR_calc1'] = 0
            
        if self.calc2_numvox != 0:
            qc['mean_calc2'] = round(self.calc2_mean / self.calc2_numvox,5) 
            qc['SNR_calc2'] = round(qc['mean_calc2'] / qc['SD'],5)
            qc['CNR_calc2'] = abs(qc['mean_calc2'] - qc['mean']) / qc['SD']
        else:
            qc['mean_calc2'] = 0
            qc['SNR_calc2'] = 0
            qc['CNR_calc2'] = 0    
        
        qc['num_vox_calc1'] = self.calc1_numvox
        qc['num_vox_calc2'] = self.calc2_numvox
        
        self.qc = qc
    
        return None
                
            