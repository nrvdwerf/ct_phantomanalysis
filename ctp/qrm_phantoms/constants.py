# File to declare all constants for CT phantom analysis
# root_path = r'C:\Users\nrvdw\LocalData\Gitlab\Test_dcm\CCI' #Surface
root_path = r'D:\DCM_data\PCCT_CAC_scoring\Alpha\PatSize_tubevolt' #Desktop_CCI
root_path = r'D:\DCM_data\PLPC\PL+monoE pairs\Rep1\Bv44f_Matrix512_QIR3_0.6mm_iodine_0mms\#monoE55_Retro_Bv44f_Matrix512_QIR3_0.6mm_iodine_0mms' #Desktop_artery
root_path = r'H:\MCR_sorted\POS1\HELIX\TS_AX_60%_0__60%' #Desktop_hollow_artery

output_path = root_path
# Definition of phantom (Thorax / Abdomen) and insert (CCI / D100 / solid_artery / hollow_artery)
phantom = 'hollow_artery'
QRM_phantom = 'Thorax'

# Definition of parameter for connected components: 4 or 8
comp_connect = 4    

# Definition if default 130HU or other threshold should be used: default or kVp_adjusted or monoE
calcium_threshold = 'default'
Volume_score_calc = False
Philips_mass_score_threshold = 130#int(100/0.88)
MTF_calc = False
Artery_blooming_calc = False
Artery_blooming_calc_trigger = ''#'#monoE55'
motion_artefact_calc = True

# Definition of used scoring method: Philips / Siemens / Canon / GE / literature / default
scoring_method = 'default'
#scoring_method = 'Canon'

print_to_excel = True
