from ctp.cirs_062 import CIRS_062
from ctp.cirs_062 import constants as C
from tqdm import tqdm
import numpy as np
np.seterr(all='ignore')
# import matplotlib.pyplot as plt
    

def main():
    cirs_analysis = CIRS_062.CIRS_062_analyser(dcm_path_list[idx], C.configuration, plot_centers = True)

    return cirs_analysis
    
    
if __name__ == '__main__':
    dcm_path_list = CIRS_062.dcm_list_builder(C.root_path)
    print('Used config = ', C.configuration)
    
    for idx in tqdm(range(len(dcm_path_list))):
        dcm = main()