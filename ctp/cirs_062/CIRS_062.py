# main document for CT phantom analysis

import pydicom
# import SimpleITK as sitk
import os
import numpy as np
# import scipy
import matplotlib.pyplot as plt
# import cv2
import math
import statistics
from ctp.Phantom_analysis_toolbox import Phantom_analysis_toolbox as pat
# from ctp.cirs_062 import constants as C

  

    
class CIRS_062_analyser(pat):
    def __init__(self, used_path, configuration, plot_centers = False):
        super().__init__(used_path)
        
        self.configuration = configuration
        
        self.large_insert_size = math.pi * (30/2)**2 / self.pixel_size**2
        self.large_insert_HU = 550
        self.small_insert_size = math.pi * (10/2)**2 / self.pixel_size**2
        self.small_insert_HU = 500
        self.air_insert_HU = -700
        self.small_air_size = math.pi * (3/2)**2 / self.pixel_size**2
        self.large_insert_ROI = int((20/2) / self.pixel_size)
        self.small_insert_ROI = int((7/2) / self.pixel_size)      
        self.CIRS_062_center_slice()
        self.WAD_input = {}
        
        #flip array if scan direction is flipped
        self.CIRS_062_flipper()
        
        self.CIRS_062_object_locater(plot_centers)
        self.CIRS_062_measurements()
        self.generate_WAD_output()
        

        return None
    
    
    def CIRS_062_center_slice(self):
        array = self.array.copy()
        
        center_slices = []
        for slice_index in range(array.shape[2]):

            sphere_centers = pat.find_circle(self.array[:,:,slice_index],\
                                             self.large_insert_HU,\
                                                 self.large_insert_size)
            
            if len(sphere_centers) > 0:
                center_slices.append(slice_index)
                
        self.center_slice = int(statistics.median(center_slices))
        self.center_min_max = [min(center_slices), max(center_slices)]
        
        return None
    
    
    def CIRS_062_flipper(self):
        center_low = self.array[int(self.header.Columns/2),int(self.header.Rows/2),5:self.array.shape[2]-5].min()
        
        index_center_low, = np.where(self.array[int(self.header.Columns/2),int(self.header.Rows/2),:] == center_low)
        
        if len(index_center_low) > 1:
            index_center_low = index_center_low[0]
        
        if index_center_low > self.center_slice:
            self.array = np.flip(self.array,2)
            self.CIRS_062_center_slice()

        return None                                    
    
    
    @staticmethod
    def calc_polar_angle(delta_x, delta_y):
        angle = math.atan(delta_y / delta_x)
                
        return angle               
    
    def high_dens_param_body1(self):
        #locate large high density object
        large_center = pat.find_circle(self.array[:,:,self.center_slice],\
                                                   self.large_insert_HU, self.large_insert_size)
            
        #locate small high density object
        small_center = pat.find_circle(self.array[:,:,self.center_slice],\
                                                  self.small_insert_HU, self.small_insert_size)
            
        air_center = [int(self.header.Columns / 2), int(self.header.Rows / 2)] 
        
        radius_small = pat.distance_between_points(large_center, air_center)
        radius_large = pat.distance_between_points(small_center, air_center)
        
        angle_offset_small = self.calc_polar_angle(large_center[0] - air_center[0],\
                                        large_center[1] - air_center[1])
            
        angle_offset_large = self.calc_polar_angle(small_center[0] - air_center[0],\
                                        small_center[1] - air_center[1])
        
        return air_center, radius_small, angle_offset_small, radius_large, angle_offset_large 
    
    
    def calc_object_centers_body1(self, center, radius_small, angle_offset_small, radius_large, angle_offset_large):
        radius_inner = radius_small
        radius_outer = radius_large
        radius_outer_bottom = radius_small + (50 / self.pixel_size) 
        
        angles_inner = []
        angles_outer = []
        for index in range(8):
            angles_inner.append(index * math.pi / 4 - angle_offset_small - math.pi / 2)
            angles_outer.append(index * math.pi / 4 - angle_offset_large - math.pi / 2)
            
        inner_object_centers = []
        outer_object_centers = []
        for angle in angles_inner:
            inner_object_centers.append([round(center[0] + (radius_inner * math.cos(angle))),\
                                         round(center[1] + (radius_inner * math.sin(angle)))])
        
        for angle in angles_outer:
            outer_object_centers.append([round(center[0] + (radius_outer * math.cos(angle))),\
                                         round(center[1] + (radius_outer * math.sin(angle)))])
                
        bottom_obj_ind = 4
        outer_object_centers[bottom_obj_ind] = [round(center[0] + (radius_outer_bottom * math.cos(angles_outer[bottom_obj_ind]))),\
                                    round(center[1] + (radius_outer_bottom * math.sin(angles_outer[bottom_obj_ind])))]
            
        self.inner_object_centers = inner_object_centers
        self.outer_object_centers = outer_object_centers
        
        return None
    

    def high_dens_param_body2(self):
        #locate large high density object
        large_center = pat.find_circle(self.array[:,:,self.center_slice],\
                                                   self.large_insert_HU, self.large_insert_size)
            
        #locate small high density object
        small_center = pat.find_circle(self.array[:,:,self.center_slice],\
                                                  self.small_insert_HU, self.small_insert_size)
            
        air_center = [int(self.header.Columns / 2), int(self.header.Rows / 2)] 
        
        radius_large = pat.distance_between_points(large_center, air_center)
        radius_small = pat.distance_between_points(small_center, air_center)
        
        angle_offset_small = self.calc_polar_angle(large_center[0] - air_center[0],\
                                        large_center[1] - air_center[1])
            
        angle_offset_large = self.calc_polar_angle(small_center[0] - air_center[0],\
                                        small_center[1] - air_center[1])
        
        return air_center, radius_small, angle_offset_small, radius_large, angle_offset_large 
    
    
    def calc_object_centers_body2(self, center, radius_small, angle_offset_small, radius_large, angle_offset_large):
        radius_inner = radius_small
        radius_outer = radius_large
        radius_outer_bottom = radius_small + (50 / self.pixel_size) 
        
        angles_inner = []
        angles_outer = []
        for index in range(8):
            angles_inner.append(index * math.pi / 4 - angle_offset_small - math.pi / 2)
            angles_outer.append(index * math.pi / 4 - angle_offset_large - math.pi / 2)
            
        inner_object_centers = []
        outer_object_centers = []
        for angle in angles_inner:
            inner_object_centers.append([round(center[0] + (radius_inner * math.cos(angle))),\
                                         round(center[1] + (radius_inner * math.sin(angle)))])
        
        for angle in angles_outer:
            outer_object_centers.append([round(center[0] + (radius_outer * math.cos(angle))),\
                                         round(center[1] + (radius_outer * math.sin(angle)))])
                
        bottom_obj_ind = 4
        outer_object_centers[bottom_obj_ind] = [round(center[0] + (radius_outer_bottom * math.cos(angles_outer[bottom_obj_ind]))),\
                                    round(center[1] + (radius_outer_bottom * math.sin(angles_outer[bottom_obj_ind])))]
            
        self.inner_object_centers = inner_object_centers
        self.outer_object_centers = outer_object_centers
        
        return None
    
    
    @staticmethod 
    def add_mask(columns, rows, obj_center, mask_in, array_max, mask_size = 3):
        
        mask_new = pat.create_circular_mask(columns, rows, obj_center , mask_size)
        mask_new = mask_new * np.ones_like(mask_new) * 2 * array_max
        
        mask_out = mask_in + mask_new
        
        return mask_out
    
    
    @staticmethod
    def object_dict_init(inner_obj, outer_obj, configuration):
        object_dict = {}
        
        if configuration == 'body1':
            dict_keys_inner = ['inner_upper_left', 'inner_top', 'inner_upper_right',\
                               'inner_right', 'inner_lower_right', 'inner_bottom',\
                                   'inner_lower_left', 'inner_left']
            dict_keys_outer = ['outer_top', 'outer_upper_right', 'outer_right',\
                               'outer_lower_right', 'outer_bottom', 'outer_lower_left',\
                                   'outer_left', 'outer_upper_left']
        elif configuration == 'body2':
            dict_keys_inner = ['inner_top', 'inner_upper_right', 'inner_right', 'inner_lower_right',\
                           'inner_bottom', 'inner_lower_left', 'inner_left', 'inner_upper_left']
            dict_keys_outer = ['outer_upper_left', 'outer_top', 'outer_upper_right',\
                               'outer_right', 'outer_lower_right', 'outer_bottom',\
                                   'outer_lower_left', 'outer_left']
                
                
        
        for index in range(len(dict_keys_inner)):
            object_dict[dict_keys_inner[index]] = [inner_obj[index]]
        
        for index in range(len(dict_keys_outer)):
            object_dict[dict_keys_outer[index]] = [outer_obj[index]]    
            
        return object_dict  

    
    def CIRS_062_object_locater(self, plot_centers = False):
        if self.configuration == 'body1':
            center, radius_small, angle_offset_small, radius_large, angle_offset_large  = self.high_dens_param_body1()
            self.calc_object_centers_body1(center, radius_small, angle_offset_small, radius_large, angle_offset_large)
            
            
        elif self.configuration == 'body2':
            center, radius_small, angle_offset_small, radius_large, angle_offset_large  = self.high_dens_param_body2()
            self.calc_object_centers_body2(center, radius_small, angle_offset_small, radius_large, angle_offset_large)
        
        array = self.array[:,:,self.center_slice].copy()
            
        mask = np.zeros_like(array)
        mask = self.add_mask(self.header.Columns, self.header.Rows, center, mask, array.max()) 
        
        for obj_center in self.inner_object_centers:
            mask = self.add_mask(self.header.Columns, self.header.Rows, obj_center, mask, array.max())

            
        for obj_center in self.outer_object_centers:
            mask = self.add_mask(self.header.Columns, self.header.Rows, obj_center, mask, array.max())
            
        self.ROI_image = array + mask
        
        if plot_centers:
            plt.subplot(131)
            plt.imshow(array)
            plt.subplot(132)
            plt.imshow(mask)
            plt.subplot(133)
            plt.imshow(array + mask)
            plt.show()
            
        self.objects = self.object_dict_init(self.inner_object_centers,\
                                             self.outer_object_centers, self.configuration)
        
        del self.inner_object_centers
        del self.outer_object_centers

        return None
    
    
    def CIRS_062_measurements(self):
        # measure mean and SD for each of the objects
        
        # slices to measure in
        min_slice = self.center_min_max[0] + 2
        max_slice = self.center_min_max[1] - 2
        
        if min_slice >= self.center_slice:
            min_slice = self.center_slice
            
        if max_slice <= self.center_slice:
            max_slice = self.center_slice
        
        for key in self.objects.keys():
            if key != 'image':
                center = self.objects[key][0]
                
                if self.configuration == 'body1':
                    if 'outer_top' in key:
                        size_obj = self.small_insert_ROI
                    else:
                        size_obj = self.large_insert_ROI
                elif self.configuration == 'body2':
                    if 'inner_top' in key:
                        size_obj = self.small_insert_ROI
                    else:
                        size_obj = self.large_insert_ROI
                
                
                mean, SD = pat.calc_mean_SD(self.array[:,:,min_slice:max_slice],\
                                                center, size_obj)
                    
                # mask = np.invert(pat.create_circular_mask(self.array.shape[0], self.array.shape[1], center, size_obj))
                # plt.imshow(mask * self.array[:,:,self.center_slice])
                # plt.show()
                
                
                self.objects[key].append(mean)
                self.objects[key].append(SD)
                self.WAD_input[key + '_mean'] = mean
                self.WAD_input[key + '_SD'] = SD
                
        return None
    
    def generate_WAD_output(self):
        self.WAD_output = {}
        self.WAD_output['ROI_image'] = self.ROI_image
        
        if self.configuration == 'body1':
            self.WAD_output['CorticalBoneCore_mean'] = self.WAD_input['outer_top_mean']
            self.WAD_output['CorticalBoneCor_SD'] = self.WAD_input['outer_top_SD']
            self.WAD_output['100%Gland_mean'] = self.WAD_input['outer_upper_left_mean']
            self.WAD_output['100%Gland_SD'] = self.WAD_input['outer_upper_left_SD']
            self.WAD_output['30%Cort_70%Trab_mean'] = self.WAD_input['outer_left_mean']
            self.WAD_output['30%Cort_70%Trab_SD'] = self.WAD_input['outer_left_SD']
            self.WAD_output['Adipose_mean'] = self.WAD_input['outer_lower_left_mean']
            self.WAD_output['Adipose_SD'] = self.WAD_input['outer_lower_left_SD']
            self.WAD_output['LungMedium_mean'] = self.WAD_input['outer_bottom_mean']
            self.WAD_output['LungMedium_SD'] = self.WAD_input['outer_bottom_SD']
            self.WAD_output['TrabBone_mean'] = self.WAD_input['outer_lower_right_mean']
            self.WAD_output['TrabBone_SD'] = self.WAD_input['outer_lower_right_SD']
            self.WAD_output['SoftTissue_mean'] = self.WAD_input['outer_right_mean']
            self.WAD_output['SoftTissue_SD'] = self.WAD_input['outer_right_SD']
            self.WAD_output['Brain_mean'] = self.WAD_input['outer_upper_right_mean']
            self.WAD_output['Brain_SD'] = self.WAD_input['outer_upper_right_SD']
            self.WAD_output['70%Gland_30%Adipose_mean'] = self.WAD_input['inner_top_mean']
            self.WAD_output['70%Gland_30%Adipose_SD'] = self.WAD_input['inner_top_SD']
            self.WAD_output['Prostate_mean'] = self.WAD_input['inner_upper_left_mean']
            self.WAD_output['Prostate_SD'] = self.WAD_input['inner_upper_left_SD']
            self.WAD_output['30%Gland_70%Adipose_mean'] = self.WAD_input['inner_left_mean']
            self.WAD_output['30%Gland_70%Adipose_SD'] = self.WAD_input['inner_left_SD']
            self.WAD_output['SpinalCord_mean'] = self.WAD_input['inner_lower_left_mean']
            self.WAD_output['SpinalCord_SD'] = self.WAD_input['inner_lower_left_SD']
            self.WAD_output['Kidney_mean'] = self.WAD_input['inner_bottom_mean']
            self.WAD_output['Kidney_SD'] = self.WAD_input['inner_bottom_SD']
            self.WAD_output['SpinalDisk_mean'] = self.WAD_input['inner_lower_right_mean']
            self.WAD_output['SpinalDisk_SD'] = self.WAD_input['inner_lower_right_SD']
            self.WAD_output['LungInhale_mean'] = self.WAD_input['inner_right_mean']
            self.WAD_output['LungInhale_SD'] = self.WAD_input['inner_right_SD']
            self.WAD_output['70%Cort_30%Trab_mean'] = self.WAD_input['inner_upper_right_mean']
            self.WAD_output['70%Cort_30%Trab_SD'] = self.WAD_input['inner_upper_right_SD']
        
        elif self.configuration == 'body2':
            self.WAD_output['70%Gland_30%Adipose_mean'] = self.WAD_input['outer_top_mean']
            self.WAD_output['70%Gland_30%Adipose_SD'] = self.WAD_input['outer_top_SD']
            self.WAD_output['Prostate_mean'] = self.WAD_input['outer_upper_left_mean']
            self.WAD_output['Prostate_SD'] = self.WAD_input['outer_upper_left_SD']
            self.WAD_output['30%Gland_70%Adipose_mean'] = self.WAD_input['outer_left_mean']
            self.WAD_output['30%Gland_70%Adipose_SD'] = self.WAD_input['outer_left_SD']
            self.WAD_output['SpinalCord_mean'] = self.WAD_input['outer_lower_left_mean']
            self.WAD_output['SpinalCord_SD'] = self.WAD_input['outer_lower_left_SD']
            self.WAD_output['Kidney_mean'] = self.WAD_input['outer_bottom_mean']
            self.WAD_output['Kidney_SD'] = self.WAD_input['outer_bottom_SD']
            self.WAD_output['SpinalDisk_mean'] = self.WAD_input['outer_lower_right_mean']
            self.WAD_output['SpinalDisk_SD'] = self.WAD_input['outer_lower_right_SD']
            self.WAD_output['LungInhale_mean'] = self.WAD_input['outer_right_mean']
            self.WAD_output['LungInhale_SD'] = self.WAD_input['outer_right_SD']
            self.WAD_output['70%Cort_30%Trab_mean'] = self.WAD_input['outer_upper_right_mean']
            self.WAD_output['70%Cort_30%Trab_SD'] = self.WAD_input['outer_upper_right_SD']
            self.WAD_output['CorticalBoneCore_mean'] = self.WAD_input['inner_top_mean']
            self.WAD_output['CorticalBoneCore_SD'] = self.WAD_input['inner_top_SD']
            self.WAD_output['100%Gland_mean'] = self.WAD_input['inner_upper_left_mean']
            self.WAD_output['100%Gland_SD'] = self.WAD_input['inner_upper_left_SD']
            self.WAD_output['30%Gland_70%Adipose_mean'] = self.WAD_input['inner_left_mean']
            self.WAD_output['30%Gland_70%Adipose_SD'] = self.WAD_input['inner_left_SD']
            self.WAD_output['Adipose_mean'] = self.WAD_input['inner_lower_left_mean']
            self.WAD_output['Adipose_SD'] = self.WAD_input['inner_lower_left_SD']
            self.WAD_output['LungMedium_mean'] = self.WAD_input['inner_bottom_mean']
            self.WAD_output['LungMedium_SD'] = self.WAD_input['inner_bottom_SD']
            self.WAD_output['TrabBone_mean'] = self.WAD_input['inner_lower_right_mean']
            self.WAD_output['TrabBone_SD'] = self.WAD_input['inner_lower_right_SD']
            self.WAD_output['SoftTissue_mean'] = self.WAD_input['inner_right_mean']
            self.WAD_output['SoftTissue_SD'] = self.WAD_input['inner_right_SD']
            self.WAD_output['Brain_mean'] = self.WAD_input['inner_upper_right_mean']
            self.WAD_output['Brain_SD'] = self.WAD_input['inner_upper_right_SD']
        
        return None
    
        
        
def dcm_list_builder(path, test_text = ""):
    # function to get list of dcm_files from dcm directory
    dcm_path_list = []
    for (dirpath, dirnames, filenames) in os.walk(path,topdown=True):
        if dirpath not in dcm_path_list:
            for filename in filenames:
                try:
                    tmp_str = str('\\\\?\\' + os.path.join(dirpath, filename))
                    pydicom.read_file(tmp_str, stop_before_pixels = True)
                    if dirpath not in dcm_path_list:
                        dcm_path_list.append(dirpath)
                except:
                    pass
            else:
                pass
    return dcm_path_list