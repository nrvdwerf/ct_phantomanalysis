from ctp.cirs_062 import CIRS_062

def main_cirs062_wad(dcm=None, manual_input=None,\
                    action_name=None, action_params=None):    
    
    if action_name == 'body1':
        configuration = 'body1'
    elif action_name == 'body2':
        configuration = 'body2'
    else:        
        raise RuntimeError('I can only analyse CIRS 062 phantom with body1 or body2 config!')

    cirs_analysis = CIRS_062.CIRS_062_analyser(dcm, configuration)

    return cirs_analysis.WAD_output
    
    
if __name__ == '__main__':
    pass